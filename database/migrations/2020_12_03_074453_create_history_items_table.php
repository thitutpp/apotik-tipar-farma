<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('medicine_id')->nullable();
            $table->foreign('medicine_id', 'medicine_id')->references('id')->on('medicines')->onDelete('set null')->onUpdate('CASCADE');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('CASCADE');
            $table->string('stock',10);
            $table->string('stockChange',10);
            $table->string('last_stock',10);
            $table->string('tipe',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_items');
    }
}
