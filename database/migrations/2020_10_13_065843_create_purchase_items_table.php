<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('medicine_id')->nullable();
            $table->foreign('medicine_id', 'medicine_id_fk')->references('id')->on('medicines')->onDelete('set null')->onUpdate('CASCADE');
            $table->unsignedInteger('invoices_number')->unsigned()->nullable();
            $table->foreign('invoices_number', 'invoices_number_fk')->references('id')->on('invoices')->onDelete('set null')->onUpdate('CASCADE');
            $table->unsignedBigInteger('stock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items');
    }
}
