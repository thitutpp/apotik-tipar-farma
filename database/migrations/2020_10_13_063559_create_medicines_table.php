<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('unit_id')->nullable();
            $table->foreign('unit_id', 'unit_fk')->references('id')->on('medicine_units')->onDelete('set null')->onUpdate('CASCADE');
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id', 'category_fk')->references('id')->on('medicine_categories')->onDelete('set null')->onUpdate('CASCADE');
            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->foreign('supplier_id', 'supplier_fk')->references('id')->on('medicine_suppliers')->onDelete('set null')->onUpdate('CASCADE');
            $table->string('code')->unique();
            $table->string('name')->nullable();
            $table->string('weight')->nullable();
            $table->integer('hpp')->nullable();
            $table->integer('price')->nullable();
            $table->string('stock')->nullable();
            $table->date('expired')->nullable();
            $table->boolean('is_expired')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines');
    }
}
