<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_id_f')->references('id')->on('users')->onDelete('set null')->onUpdate('CASCADE');
            $table->unsignedInteger('expense_category_id')->nullable();
            $table->foreign('expense_category_id', 'expense_category_id_fk')->references('id')->on('expense_categories')->onDelete('set null')->onUpdate('CASCADE');
            $table->date('entry_date')->nullable();
            $table->integer('amount')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
