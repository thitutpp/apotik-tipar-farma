<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicineReturTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_retur', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('medicine_id')->nullable();
            $table->foreign('medicine_id', 'medicine_id_')->references('id')->on('medicines')->onDelete('set null')->onUpdate('CASCADE');
            $table->unsignedInteger('retur_category_id')->nullable();
            $table->foreign('retur_category_id', 'retur_category_id_fk')->references('id')->on('medicine_retur_categories')->onDelete('set null')->onUpdate('CASCADE');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_id_')->references('id')->on('users')->onDelete('set null')->onUpdate('CASCADE');
            $table->string('qty_return')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicine_retur');
    }
}
