<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateIncomeCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('income_categories')->insert([
                [
                    'name'            => 'Penjualan',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Dana Hibah',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
            ]);
    }
}
