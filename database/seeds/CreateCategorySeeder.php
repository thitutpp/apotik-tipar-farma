<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicine_categories')->insert([
                [
                    'name'            => 'Psikotropika',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Narkotika',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Bebas Terbatas',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Bebas',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Keras',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Wajib Apotek',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Herbal',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],


            ]);
    }
}
