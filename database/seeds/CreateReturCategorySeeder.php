<?php

use Illuminate\Database\Seeder;

class CreateReturCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicine_retur_categories')->insert([
            [
                'name'            => 'Obat Kadaluarsa',
                'created_at'      => \Carbon\Carbon::now(),
                'updated_at'      => \Carbon\Carbon::now()
            ],

        ]);
    }
}
