<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateUnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicine_units')->insert([
                [
                    'name'            => 'Pulvis (Serbuk)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Pulveres',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Tablet Kempa',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Tablet Cetak',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Tablet Trikurat',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Tablet Hipodermik',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Tablet Sublingual',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Tablet Bukal',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Tablet Effervescent',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Tablet Kunyah',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Pil (Pilulae)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Kapsul (Capsule)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Kaplet (Kapsul Tablet)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Larutan (Solutiones)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Suspensi (Suspensiones)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Emulsi (Elmusiones)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Galenik',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Ekstrak (Extractum)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Infusa',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Imunoserum (Immunosera)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Salep (Unguenta)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Suppositoria',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Obat tetes (Guttae)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Injeksi (Injectiones)',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
            ]);
    }
}
