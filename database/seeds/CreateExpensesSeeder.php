<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateExpensesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expenses')->insert([
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-01-01',
                    'amount'                => '1000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-02-01',
                    'amount'                => '2000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-03-01',
                    'amount'                => '3000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-04-01',
                    'amount'                => '4000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-05-01',
                    'amount'                => '5000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-06-01',
                    'amount'                => '6000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-07-01',
                    'amount'                => '7000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-08-01',
                    'amount'                => '8000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-09-01',
                    'amount'                => '9000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-10-01',
                    'amount'                => '10000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-11-01',
                    'amount'                => '11000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '1',
                    'entry_date'            => '2020-12-01',
                    'amount'                => '12000000',
                    'description'           => 'Yo payu yo',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],

                // ------------ 2

                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-01-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-02-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-03-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-04-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-05-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-06-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-07-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-08-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-09-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-10-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-11-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'expense_category_id'   => '2',
                    'entry_date'            => '2020-12-01',
                    'amount'                => '500000',
                    'description'           => 'hehehe',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
            ]);
    }
}
