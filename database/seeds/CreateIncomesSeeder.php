<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateIncomesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('incomes')->insert([
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-01-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-02-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-03-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-04-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-05-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-06-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-07-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-08-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-09-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-10-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-11-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '1',
                    'entry_date'            => '2020-12-01',
                    'amount'                => '50000000',
                    'description'           => 'Alhamdulillah Payu',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],

                // ------------ 2

                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-01-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-02-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-03-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-04-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-05-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-06-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-07-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-08-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-09-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-10-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-11-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
                [
                    'income_category_id'   => '2',
                    'entry_date'            => '2020-12-01',
                    'amount'                => '10000000',
                    'description'           => 'Alhamdulillah Rejeki Anak Soleh',
                    'created_at'            => \Carbon\Carbon::now(),
                    'updated_at'            => \Carbon\Carbon::now()
                ],
            ]);
    }
}
