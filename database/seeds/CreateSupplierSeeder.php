<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateSupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medicine_suppliers')->insert([
            [
                'name'            => 'PT. Kimia Farma',
                'created_at'      => \Carbon\Carbon::now(),
                'updated_at'      => \Carbon\Carbon::now()
            ],
            [
                'name'            => 'PT. Libracal Pharmaceutical Industries',
                'created_at'      => \Carbon\Carbon::now(),
                'updated_at'      => \Carbon\Carbon::now()
            ],
            [
                'name'            => 'PT. Dian Kimia Putera',
                'created_at'      => \Carbon\Carbon::now(),
                'updated_at'      => \Carbon\Carbon::now()
            ],
            [
                'name'            => 'PT. Tunggal Daya',
                'created_at'      => \Carbon\Carbon::now(),
                'updated_at'      => \Carbon\Carbon::now()
            ],
            [
                'name'            => 'PT. Adi Putra',
                'created_at'      => \Carbon\Carbon::now(),
                'updated_at'      => \Carbon\Carbon::now()
            ],
            
        ]);
    }
}
