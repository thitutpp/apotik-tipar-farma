<?php

use Illuminate\Database\Seeder;
use App\Model\Permission;
use App\Model\Role;
use App\Model\User;


class DatabaseSeeder extends Seeder
{

    public function run()
    {
        // meminta konfirmasi untuk migrate refresh
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?')) {
            // memanggil php artisan migrate:refresh
            $this->command->call('migrate:refresh');
            $this->command->warn("Data cleared, starting from blank database.");
        }

        $this->call(CreateSupplierSeeder::class);
        $this->call(CreateUnitsSeeder::class);
        $this->call(CreateCategorySeeder::class);
        $this->call(CreateMedicineSeeder::class);
        $this->call(CreateIncomeCategoriesSeeder::class);
        $this->call(CreateExpenseCategoriesSeeder::class);
        // $this->call(CreateIncomesSeeder::class);
        // $this->call(CreateExpensesSeeder::class);
        $this->call(CreateSupplierSeeder::class);
        $this->call(CreateReturCategorySeeder::class);

        // Memberikan default permission
        $permissions = Permission::adminPermissions();

        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }

        $this->command->info('Default Permissions added.');

        // meminta konfirmasi penambahan user
        if ($this->command->confirm('Create Roles for user, default is admin and user? [y|N]', true)) {

            // menginputkan peranan yang dibutuhkan
            $input_roles = $this->command->ask('Enter roles in comma separate format.', 'Admin,Kasir');

            // menampung $input_roles menjadi array. contoh : ['Admin', 'Kasir']
            $roles_array = explode(',', $input_roles);

            // Menambahkan peranan
            foreach ($roles_array as $role) {
                $role = Role::firstOrCreate(['name' => trim($role)]);

                if ($role->name == 'Admin') {
                    // mensinkronkan peranan admin
                    $role->syncPermissions(Permission::adminPermissions());
                    $this->command->info('Admin granted all the permissions');
                } else if ($role->name == 'Kasir') {
                    //  mensinkronkan peranan kasir

                    $role->syncPermissions(Permission::cashierPermissions());
                    $this->command->info('Role ' . $input_roles . ' already has their respective permissions');
                }

                // membuat user pertama kali yang telah diinputkan dan ditampung di $roles_array
                $this->createUser($role);
            }

            $this->command->info('Roles ' . $input_roles . ' added successfully');
        } else {
            Role::firstOrCreate(['name' => 'User']);
            $this->command->info('Added only default user role.');
        }
    }

    private function createUser($role)
    {
        $user = factory(User::class)->create();
        $user->assignRole($role->name);

        if ($role->name == 'Admin') {
            $this->command->info('Here is your admin details to login:');
            $this->command->warn($user->email);
            $this->command->warn('Password is "password"');
        }
    }
}
