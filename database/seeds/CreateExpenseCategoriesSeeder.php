<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateExpenseCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expense_categories')->insert([
                [
                    'name'            => 'Modal',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
                [
                    'name'            => 'Transport',
                    'created_at'      => \Carbon\Carbon::now(),
                    'updated_at'      => \Carbon\Carbon::now()
                ],
            ]);
    }
}
