<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CreateMedicineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 10; $i++) {
            $satuanBerat = ['Kg', 'Gram', 'Mili'];
            $index = $faker->numberBetween(0, 2);
            $berat = $faker->numberBetween(60, 1000);
            // insert data ke table pegawai menggunakan Faker
            DB::table('medicines')->insert([
                'unit_id' => $faker->numberBetween(1, 24),
                'category_id' => $faker->numberBetween(1, 7),
                'supplier_id' => $faker->numberBetween(1, 5),
                'price' => $faker->numberBetween(4000, 12000),
                'code' => $faker->unique()->numberBetween(10, 9999999),
                'name' => $faker->name,
                'weight' =>  $faker->numberBetween(60, 1000),
                'hpp' => $faker->numberBetween(1000, 4000),
                'price' => $faker->numberBetween(4000, 12000),
                'stock' => $faker->numberBetween(25, 40),
                'expired' => $faker->date(),
                'is_expired' => FALSE,
                'created_at' => $faker->date(),
                'updated_at' => $faker->date(),
            ]);
        }
    }
}
