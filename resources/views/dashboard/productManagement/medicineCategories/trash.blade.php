@extends('layouts.dashboard')
@section('title', 'Tong Sampah Kategori')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Tong Sampah</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('medicine-categories.index')}}" class="text-muted">Kategori Obat</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tong Sampah</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

@if(Session::has('success'))
<div class="page-breadcrumb">
    @include('layouts.flash-success',[ 'message'=> Session('success') ])
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sampah</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-dark" href="{{ route('medicine-categories.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th width="10">
                                        No
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($medicineCategories as $medicineCategory)
                                <tr data-entry-id="{{ $medicineCategory->id }}">
                                    <td>
                                        {{ $no++ ?? '' }}
                                    </td>
                                    <td>
                                        {{ $medicineCategory->name ?? '' }}
                                    </td>

                                    <td>
                                        <a class="btn btn-xs btn-primary" href="{{ route('medicine-category.restore', $medicineCategory->id) }}">
                                            <i class="far fa-window-restore"> Pulihkan</i>
                                        </a>
                                        <a class="btn btn-xs btn-danger" href="{{ route('medicine-category.permanentDelete', $medicineCategory->id) }}">
                                            <i class="far fa-trash-alt"> Hapus Permanen</i>
                                        </a>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection