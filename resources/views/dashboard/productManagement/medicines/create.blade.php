@extends('layouts.dashboard')
@section('title', 'Tambah Obat')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Tambah Obat</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('medicines.index')}}" class="text-muted">Obat</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Obat</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Form Tambah Obat</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('medicines.store')}}">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="required" for="code">Kode</label>
                                        <input class="form-control" type="text" name="code" id="code" value="{{ old('code', '') }}">


                                    </div>
                                    <div class="form-group ">
                                        <label for="name">Nama</label>
                                        <input class="form-control" type="text" name="name" id="name" value="{{ old('name', '') }}">


                                    </div>
                                    <div class="form-group ">
                                        <label for="weight">Berat</label>
                                        <input class="form-control" type="text" name="weight" id="weight" value="{{ old('weight', '') }}" step="1" placeholder="100 Gram / 100 ML">


                                    </div>
                                    <div class="form-group ">
                                        <label for="hpp">HPP</label>
                                        <input class="form-control" type="number" name="hpp" id="hpp" value="{{ old('hpp', '') }}" step="0.01">


                                    </div>
                                    <div class="form-group ">
                                        <label for="price">Harga</label>
                                        <input class="form-control" type="number" name="price" id="price" value="{{ old('price', '') }}" step="0.01">


                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="stock">Stok</label>
                                        <input class="form-control" type="number" name="stock" id="stock" value="{{ old('stock', '') }}">


                                    </div>
                                    <div class="form-group">
                                        <label for="category_id">Perusahaan Pemasok</label>
                                        <select class="form-control select2" name="supplier_id" id="supplier_id">
                                            @foreach($suppliers as $id => $supplier)
                                            <option value="{{ $id }}">{{ $supplier }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group ">
                                        <label for="unit_id">Satuan</label>
                                        <select class="form-control select2" name="unit_id" id="unit_id">
                                            @foreach($units as $id => $unit)
                                            <option value="{{ $id }}" {{ old('unit_id') == $id ? 'selected' : '' }}>{{ $unit }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="category_id">Kategori</label>
                                        <select class="form-control select2" name="category_id" id="category_id">
                                            @foreach($categories as $id => $category)
                                            <option value="{{ $id }}">{{ $category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="category_id">Tanggal Kadaluarsa</label>
                                        <input type="date" class="form-control" name="expired" id="expired" value="{{ old('expired', '') }}">
                                    </div>
                                    <div class="form-actions mt-5">
                                        <button class="btn btn-success" type="submit">
                                            <i class="far fa-save"> Simpan</i>
                                        </button>
                                        <button type="reset" class="btn btn-danger">
                                            <i class="far fa-window-close"> Setel Ulang</i>
                                        </button>
                                        <a class="btn btn-dark" href="{{ route('medicines.index') }}">
                                            <i class="far fa-arrow-alt-circle-left"> Kembali</i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection