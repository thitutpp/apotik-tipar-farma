@extends('layouts.dashboard')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Basic Initialisation</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Library</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Zero Configuration</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-success" href="{{ route('medicines.create') }}">
                                Add Medicine
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <tbody>
                                <tr>
                                    <th>
                                        Id
                                    </th>
                                    <td>
                                        {{ $medicine->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Code
                                    </th>
                                    <td>
                                        {{ $medicine->code }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <td>
                                        {{ $medicine->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Weight
                                    </th>
                                    <td>
                                        {{ $medicine->weight }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Price
                                    </th>
                                    <td>
                                        {{ $medicine->price }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Stock
                                    </th>
                                    <td>
                                        {{ $medicine->stock }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Unit
                                    </th>
                                    <td>
                                        {{ $medicine->unit->name ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Category
                                    </th>
                                    <td>
                                        {{ $medicine->category->name ?? '' }}
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('medicines.index') }}">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection