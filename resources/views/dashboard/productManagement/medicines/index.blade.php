@extends('layouts.dashboard')
@section('title', 'Obat')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Obat</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Obat</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

@if(Session::has('success'))
<div class="page-breadcrumb">
    @include('layouts.flash-success',[ 'message'=> Session('success') ])
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Obat</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">

                            <a class="btn btn-success" href="{{ route('medicines.create') }}">
                                <i class="far fa-plus-square"> Tambah Obat</i>
                            </a>
                            <a class="btn btn-dark" href="{{ route('medicine.trash') }}">
                                <i class="far fa-trash-alt"> Tong Sampah</i>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="medicine" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Kode
                                    </th>
                                    <th>
                                        Nama
                                    </th>
                                    <th>
                                        Berat
                                    </th>
                                    <th>
                                        Satuan
                                    </th>
                                    <th>
                                        HPP
                                    </th>
                                    <th>
                                        Harga
                                    </th>
                                    <th>
                                        Stok
                                    </th>
                                    <th>
                                        Perusahaan Pemasok
                                    </th>
                                    
                                    <th>
                                        Kategori
                                    </th>
                                    <th class="text-center">
                                        Tindakan
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div id="confirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="danger-header-modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-colored-header bg-danger">
                <h4 class="modal-title" id="danger-header-modalLabel">Perhatian !!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin untuk mengahapus data obat?.</p>
                <i>Data yang telah terhapus, secara otomatis akan langsung berpindah ke tong sampah.
                    Hanya pihak Admin yang dapat memulihkan kembali data yang telah terhapus.</i>

            </div>
            <div class="modal-footer">
                <button type="button" id="ok_button" name="ok_button" class="btn btn-danger">Ya</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div> -->
@endsection 

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#medicine').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('medicines.index') }}",
            columns: [
                {data:'id',  render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }},
                {data:'code'},
                {data:'name'},
                {data:'weight'},
                {data:'unit',  name:'unit.name'},
                {data:'hpp'},
                {data:'price'},
                {data:'stock'},
                {data:'supplier', name:'supplier.name'},
                {data:'category', name:'category.name'},
                {data:'action', name: 'action', orderable: false, className: 'text-xl-center'},
            ]
        });

        // $(document).on('click', '.delete', function() {
        //     medicine_id = $(this).attr('id');
        //     $('#confirmModal').modal('show');
        // });

        // $('#ok_button').click(function() {
        //     $.ajax({
        //         url: "medicine/delete/" + medicine_id,
        //         beforeSend: function() {
        //             $('#ok_button').text('Harap tunggu...');
        //         },
        //         success: function(medicine) {
        //             setTimeout(function() {
        //                 $('#confirmModal').modal('hide');
        //                 $('#medicine').DataTable().ajax.reload();
        //             }, 1000);
        //         }
        //     })
        // });

        $(document).on('click', '.delete', function() {
            var medicine_id = $(this).attr('id');
            if (confirm("Apakah anda yakin ?")) {
                $.ajax({
                    url: "medicine/delete/" + medicine_id,
                    mehtod: "get",
                    success: function() {
                        alert("Data obat berhasil dihapus");
                        $('#medicine').DataTable().ajax.reload();
                    }
                })
            }
        });
    });
</script>
@endpush