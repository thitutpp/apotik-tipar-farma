@extends('layouts.dashboard')
@section('title', 'Edit Obat')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Ubah Obat</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('medicines.index')}}" class="text-muted">Obat</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Ubah Obat</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Ubah Obat</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('medicines.update', $medicine->id) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="id" value="{{ $medicine->id }}">
                                <input type="hidden" name="stock" value="{{ $medicine->stock }}">
                                <div class="form-group">
                                    <label for="name">Nama</label>
                                    <input class="form-control" type="text" name="name" id="name" value="{{ old('name', $medicine->name) }}">

                                </div>
                                <div class="form-group">
                                    <label for="weight">Berat</label>
                                    <input class="form-control" type="text" name="weight" id="weight" value="{{ old('weight', $medicine->weight) }}" step="1">

                                </div>
                                <div class="form-group">
                                    <label for="hpp">HPP</label>
                                    <input class="form-control" type="number" name="hpp" id="hpp" value="{{ old('hpp', $medicine->hpp) }}" step="0.01">

                                </div>
                                <div class="form-group">
                                    <label for="price">Harga</label>
                                    <input class="form-control" type="number" name="price" id="price" value="{{ old('Rp price', $medicine->price) }}" step="0.01">
                                </div>
                                <div class="form-group">
                                    <label for="expired">Tanggal Kadaluarsa</label>
                                    <input type="date" class="form-control" name="expired" id="expired" value="{{ old('expired', $medicine->expired) }}">
                                </div>
                                <div class="form-group mt-5">
                                    <button class="btn btn-success" type="submit">
                                        <i class="far fa-save"> Simpan</i>
                                    </button>
                                    <button type="reset" class="btn btn-danger">
                                        <i class="far fa-window-close"> Reset</i>
                                    </button>
                                    <a class="btn btn-dark" href="{{ route('medicines.index') }}">
                                        <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                                    </a>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="stock">Stok</label>
                                    <input class="form-control" type="text" name="stock" id="stock" value="{{ old('stock', $medicine->stock) }}" disabled>
                                </div>

                                <div class="form-group ">
                                    <label for="addStock">Tambah / Kurangi Stok </label>
                                    <input type="number" class="form-control" name="addStock" value="{{ old('addStock') }}" placeholder="Use positive number to increase | negative number to decrease">
                                </div>
                                <div class="form-group">
                                    <label for="unit_id">Perusahaan Pemasok</label>
                                    <select class="form-control select2" name="supplier_id" id="supplier_id">
                                        @foreach($suppliers as $id => $supplier)
                                        <option value="{{ $id }}" {{ (old('supplier_id') ? old('supplier_id') : $medicine->supplier->id ?? '') == $id ? 'selected' : '' }}>{{ $supplier }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="unit_id">Satuan</label>
                                    <select class="form-control select2" name="unit_id" id="unit_id">
                                        @foreach($units as $id => $unit)
                                        <option value="{{ $id }}" {{ (old('unit_id') ? old('unit_id') : $medicine->unit->id ?? '') == $id ? 'selected' : '' }}>{{ $unit }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category_id">Kategori</label>
                                    <select class="form-control select2" name="category_id" id="category_id">
                                        @foreach($categories as $id => $category)
                                        <option value="{{ $id }}" {{ (old('category_id') ? old('category_id') : $medicine->category->id ?? '') == $id ? 'selected' : '' }}>{{ $category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card border-secondary">
                <div class="card-header bg-secondary">
                    <h4 class="mb-0 text-white">History Obat</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Updated By</th>
                                    <th>Stock Before</th>
                                    <th>Stock Added/Reduce</th>
                                    <th>Stock</th>
                                    <th>Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($history as $index=>$item)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td>{{$item->user->name}}</td>
                                    <td>{{$item->stock}}</td>
                                    <td>{{$item->stockChange}}</td>
                                    <td>{{$item->stock + $item->stockChange}}</td>
                                    <td>{{$item->created_at->diffForHumans()}}</td>
                                    <td>{{$item->tipe}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection