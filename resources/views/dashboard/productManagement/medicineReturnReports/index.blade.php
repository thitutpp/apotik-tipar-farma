@extends('layouts.dashboard')
@section('title', 'Laporan Retur')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Laporan Retur Obat</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Laporan Retur</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col col-6">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Telusuri Berdasarkan Jangka Waktu</h3>
                    <form method="GET" action="{{ route('medicine-return-report.search') }}">
                        @csrf
                        <div class="form-group">
                            <label class="required" for="range">Tentukan Jangka Waktu</label>
                            <input type="text" class="form-control" name="daterange" value="">
                        </div>
                        <div class="form-actions mt-5">
                            <button class="btn btn-success" type="submit">
                                <i class="fab fa-sistrix"> Cari</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(isset($returns))
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Laporan Retur</h3>
                    <div class="row">
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <th>Tanggal Obat Diretur</th>
                                        <th>Nama Obat</th>
                                        <th>Kategori Retur</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    @foreach($returns as $return)
                                    <tr>
                                        <td>{{ $return->created_at->toDateString() ?? ''}}</td>
                                        <td>{{ $return->medicine->name ?? ''}}</td>
                                        <td rowspan="">{{ $return->retur_category->name ?? ''}}</td>
                                        <td>{{ $return->qty_return ?? ''}}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    <a href="{{ url('dashboard/medicine-return-report/print_pdf/'.$from.'/'.$to)}}" class="btn btn-primary" target="_blank">CETAK PDF</a>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>

@endsection
@push('scripts')
<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'right'
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('H:i:s') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>
@endpush