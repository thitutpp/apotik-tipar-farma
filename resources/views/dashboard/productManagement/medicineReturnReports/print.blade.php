<html>

<head>
    <title>Print Laporan</title>

</head>

<style>
    .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        text-align: center;
    }
</style>

<body>
    <table cellspacing="0" border="0" width="100%">
        <tbody>
            <tr>
                <td valign="middle" align="left"><b>
                        <font size="3" color="black">APOTIK TIPAR FARMA</font>
                        <br>
                        <font size="1" color="black">Jl. Tipar Tengah No 10, Depok</font>
                    </b></td>
                <td valign="middle" align="right">
                    <img src="{{public_path('/dashboard/assets/images/TiparFarma_logo.png')}}"> <img src="{{public_path('/dashboard/assets/images/TiparFarma_text.png')}}">

                </td>
            </tr>
        </tbody>
    </table>
    <hr style="color: gray;">
    <p></p>
    <center>
        <h1>Laporan Obat Retur </h1>
        <p>Tanggal {{$from2}} - {{$to2}} </p>
    </center>



    <!-- ********************************** Retur **************************************** -->
    <table cellspacing="0" border="0" width="100%">
        <colgroup width="75"></colgroup>
        <colgroup width="176"></colgroup>
        <colgroup width="431"></colgroup>
        <tbody>
            <tr>
                <td colspan="8" bgcolor="#5f76e8" align="center"><b>
                        <font size="4" face="Times New Roman" color="#FFFFFF">Daftar Retur</font>
                    </b></td>
            </tr>
            <tr>
                <td colspan="2" valign="middle" bgcolor="#F2F2F2" align="left">
                    <b>
                        <font face="Times New Roman">Tanggal Pengembalian</font>
                    </b>
                </td>
                <td colspan="1" valign="middle" bgcolor="#F2F2F2" align="left">
                    <b>
                        <font face="Times New Roman">Nama Obat</font>
                    </b>
                </td>
                <td colspan="2" valign="middle" bgcolor="#F2F2F2" align="left">
                    <b>
                        <font face="Times New Roman">Pemasok</font>
                    </b>
                </td>
                <td colspan="3" valign="middle" bgcolor="#F2F2F2" align="right"><b>
                        <font face="Times New Roman"> Jumlah </font>
                    </b></td>
                    
            </tr>
            @foreach($returns as $return)
            <tr>
                <td colspan="2" valign="middle" align="left">
                    <font face="Times New Roman">{{ $return->created_at->toDateString() ?? ''}}</font>
                </td>
                <td colspan="1" valign="middle" align="left">
                    <font face="Times New Roman">{{ $return->medicine->name ?? ''}}</font>
                </td>
                <td colspan="2" valign="middle" align="left">
                    <font face="Times New Roman">{{ $return->medicine->supplier->name ?? ''}}</font>
                </td>
                <td colspan="3" valign="middle" align="right">
                    <font face="Times New Roman"> {{ $return->qty_return ?? ''}} </font>
                </td>
                
            </tr>
            @endforeach
            <tr>
            <td style="border-top: 1px solid #000000" colspan="8" valign="middle" align="left"></td>
            </tr>
        </tbody>
    </table>
    <!-- ********************************** Retur **************************************** -->
    <div class="footer" style="padding-top: 10px; border-top: 1px solid #ddd; font-size:13px; color: #333; margin-top: 20px; text-align: center;">
        <font size="3" color="black">APOTIK TIPAR FARMA </font>
        <br>
        <font size="1" color="black">Jl. Tipar Tengah No 10, Depok</font>
    </div>
</body>

</html>