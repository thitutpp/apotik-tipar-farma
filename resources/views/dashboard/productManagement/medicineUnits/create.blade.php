@extends('layouts.dashboard')
@section('title', 'Tambah Satuan Obat')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Tambah Satuan Obat</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('medicine-units.index')}}" class="text-muted">Satuan Obat</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Satuan Obat</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Tambah Satuan Obat</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('medicine-units.store') }}">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Satuan obat baru">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="text-left">
                                <button type="submit" class="btn btn-success">
                                    <i class="far fa-save"> Simpan</i>
                                </button>
                                <button type="reset" class="btn btn-danger">
                                    <i class="far fa-window-close"> Setel Ulang</i>
                                </button>
                                <a class="btn btn-dark" href="{{ route('medicine-units.index') }}">
                                    <i class="far fa-arrow-alt-circle-left"> Kembali</i>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection