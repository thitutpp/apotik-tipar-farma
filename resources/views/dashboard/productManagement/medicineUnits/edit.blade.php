@extends('layouts.dashboard')
@section('title', 'Edit Satuan Obat')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Edit Satuan Obat</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('medicine-units.index')}}" class="text-muted">Satuan Obat</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Ubah Satuan Obat</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Ubah Satuan Obat</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('medicine-units.update', [$medicineUnits->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ old('name', $medicineUnits->name) }}">
                            @if($errors->has('name'))
                            <span class="help-block" role="alert">{{ $errors->first('name') }}</span>
                            @endif

                        </div>
                        <div class="form-actions">
                            <button class="btn btn-success" type="submit">
                                <i class="far fa-save"> Simpan</i>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <i class="far fa-window-close"> Setel Ulang</i>
                            </button>
                            <a class="btn btn-dark" href="{{ route('medicine-units.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection