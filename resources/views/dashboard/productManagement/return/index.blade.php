@extends('layouts.dashboard')
@section('title', 'Retur Obat')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Retur</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Retur</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@if(Session::has('success'))
<div class="page-breadcrumb">
    @include('layouts.flash-success',[ 'message'=> Session('success') ])
</div>
@endif
<div class="page-breadcrumb d-flex ml-auto">
    <a href="{{route('medicine-return.check.expired')}}">
        <button class="btn btn-warning" style="margin-right:20;">
            <i class="fas fa-undo"> Cek Obat Kadaluarsa</i>
        </button>
    </a>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Obat Kadaluarsa</h4>
                    <div style="margin-bottom: 10px;" class="row">
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Kode
                                    </th>
                                    <th>
                                        Nama
                                    </th>
                                    <th>
                                        Berat
                                    </th>
                                    <th>
                                        Tanggal Kadaluarsa
                                    </th>
                                    <th>
                                        Harga
                                    </th>
                                    <th>
                                        Stok
                                    </th>
                                    <th>
                                        Satuan
                                    </th>
                                    <th>
                                        Kategori
                                    </th>
                                    <th class="text-center">
                                        Tindakan
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($medicines as $medicine)
                                <tr data-entry-id="{{ $medicine->id }}">

                                    <td>
                                        {{ ++$no }}
                                    </td>
                                    <td>
                                        {{ $medicine->code ?? '' }}
                                    </td>
                                    <td>
                                        {{ $medicine->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $medicine->weight ?? '' }}
                                    </td>
                                    <td>
                                        {{ $medicine->expired ?? '' }}
                                    </td>
                                    <td>
                                        {{ $medicine->price ?? '' }}
                                    </td>
                                    <td>
                                        {{ $medicine->stock ?? '' }}
                                    </td>
                                    <td>
                                        {{ $medicine->unit->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $medicine->category->name ?? '' }}
                                    </td>
                                    <td class="text-center">
                                        <a style="color:white" class="btn btn-xs btn-warning" onclick="return confirm('Dengan melakukan konfirmasi ini, maka anda akan membuat record data retur dengan status obat expired. Apa anda yakin?');" href="{{ route('medicine-return.all.expired', $medicine->id) }}">
                                            <i class="fas fa-sync-alt"> Perbarui Status</i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Formulir Retur Obat</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-success" href="{{ route('medicine-return.create') }}">
                                <i class="far fa-plus-square"> Tambah Retur Obat</i>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="medicine-return" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Kode
                                    </th>
                                    <th>
                                        Nama Obat Diretur
                                    </th>
                                    <th>
                                        Kuantitas Obat Diretur
                                    </th>
                                    <th>
                                        Satuan
                                    </th>
                                    <th>
                                        Tipe Pengembalian
                                    </th>
                                    <th>
                                        Supplier
                                    </th>
                                    <th>
                                        Tanggal Penginputan Data
                                    </th>
                                    <th class="text-center">
                                        Tindakan
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#medicine-return').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('medicine-return.index') }}",
            columns: [{
                    data: 'id'
                },
                {
                    data: 'code'
                },
                {
                    data: 'name'
                },
                {
                    data: 'qty_return'
                },
                {
                    data: 'unit',
                    name: 'unit.name'
                },
                {
                    data: 'retur_category',
                    name: 'category.name'
                },
                {
                    data: 'supplier',
                    name: 'supplier.name'
                },
                {
                    data: 'created_at',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    className: 'text-xl-center'
                },
            ]
        });
    });
</script>
@endpush