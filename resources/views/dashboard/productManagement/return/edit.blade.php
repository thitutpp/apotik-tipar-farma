@extends('layouts.dashboard')
@section('title', 'Edit Retur')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Ubah Retur</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('medicine-return.index')}}" class="text-muted">Retur</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Ubah Retur</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="page-breadcrumb d-flex ml-auto">
    <form method="POST" action="{{ route('medicine-return.rollback', $retur->id)}}" onsubmit="return confirm('Dengan melakukan konfirmasi ini, maka akan mengembalikan kuantitas obat yang salah diretur tanpa tanggal expired yang sebelumnya.');">
        @method('GET')
        @csrf
        <input type="hidden" name="rollback" value="{{$retur->id}}">
        <button class="btn btn-warning" type="submit" style="margin-right:20;">
            <i class="fas fa-undo"> Tetapkan Sebagai Salah Retur</i>
        </button>
    </form>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Ubah Retur</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('medicine-return.update', $retur->id)}}">
                        @method('PUT')
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="hidden" name="id" value="{{$retur->id}}">
                                        <label for="name">Obat Diretur</label>
                                        <input class="form-control" type="text" name="medicine" id="medicine" value="{{ $retur->medicine->name }}" disabled>
                                    </div>
                                    <div class="form-group ">
                                        <label for="qty_return">Stok Obat Saat Ini</label>
                                        <input class="form-control" type="number" name="stock_now" id="stock_now" value="{{$retur->medicine->stock}}" disabled>
                                    </div>
                                    <div class="form-group ">
                                        <label for="qty_return">Stok Yang Telah Diretur</label>
                                        <input class="form-control" type="number" name="qty_return" id="qty_return" value="{{$retur->qty_return}}" disabled>
                                    </div>
                                    <div class="form-actions mt-5">
                                        <button class="btn btn-success" type="submit">
                                            <i class="far fa-save"> Simpan</i>
                                        </button>
                                        <button type="reset" class="btn btn-danger">
                                            <i class="far fa-window-close"> Setel Ulang</i>
                                        </button>
                                        <a class="btn btn-dark" href="{{ route('medicine-return.index') }}">
                                            <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="qty_return">Tambah / Kurangi Stok Yang Telah Diretur</label>
                                        <input class="form-control" type="number" name="qty_return" id="qty_return" value="" placeholder="Use positive number to increase | negative number to decrease">
                                    </div>
                                    <div class="form-group">
                                        <label for="retur_category_id">Jenis Retur</label>
                                        <select class="form-control select2" name="retur_category_id" id="retur_category_id">
                                            @foreach($returCategory as $id => $category)
                                            <option value="{{ $id }}" {{ (old('retur_category_id') ? old('retur_category_id') : $retur->retur_category->id ?? '') == $id ? 'selected' : '' }}>{{ $category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection