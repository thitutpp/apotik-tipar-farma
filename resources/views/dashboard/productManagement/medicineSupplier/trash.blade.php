@extends('layouts.dashboard')
@section('title', 'Tong Sampah Perusahaan Pemasok Obat')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Tong Sampah Perusahaan Pemasok Obat</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('medicine-suppliers.index')}}" class="text-muted">Perusahaan Pemasok Obat</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tong Sampah Perusahaan Pemasok Obat</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

@if(Session::has('success'))
<div class="page-breadcrumb">
    @include('layouts.flash-success',[ 'message'=> Session('success') ])
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sampah</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-dark" href="{{ route('medicine-suppliers.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th width="10">
                                        No
                                    </th>
                                    <th>
                                        Nama
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($suppliers as $suppliers)
                                <tr data-entry-id="{{ $suppliers->id }}">
                                    <td>
                                        {{ ++$no ?? '' }}
                                    </td>
                                    <td>
                                        {{ $suppliers->name ?? '' }}
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-primary" href="{{ route('medicine-supplier.restore', $suppliers->id) }}">
                                            <i class="far fa-window-restore"> Pulihkan</i>
                                        </a>
                                        <a class="btn btn-xs btn-danger" href="{{ route('medicine-supplier.permanentDelete', $suppliers->id) }}">
                                            <i class="far fa-trash-alt"> Hapus Permanen</i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection