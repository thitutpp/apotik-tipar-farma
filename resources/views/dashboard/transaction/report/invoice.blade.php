@extends('layouts.pos')
@section('title', 'Print Nota')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium"> Nota</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('sales.index') }}" class="text-muted">Kasir</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('sales.history') }}" class="text-muted">Riwayat Transaksi</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Nota</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-lg-5 text-right">
            <a class="btn btn-xs btn-light" href="{{ route('home.index') }}"><i class="fas fa-hospital"></i><span class="hide-menu"> Dashboard</span></a>
            <a class="btn btn-xs btn-light" href="{{ route('sales.index') }}"><i class="fas fa-cart-arrow-down"></i><span class="hide-menu"> Kasir</span></a>
            <a class="btn btn-xs btn-light" href="{{ route('sales.history') }}"><i class="fas fa-dollar-sign"></i><span class="hide-menu"> Riwayat Transaksi</span></a>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card" style="min-height: 85vh">
                <div class="bg-white card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="font-weight-bold">Report / Laporan invoice</h4>
                        </div>
                        <div class="col">
                            <a href="{{ route('sales.history') }}" class="float-right btn btn-dark btn-sm"><i class="fas fa-arrow-left"></i> Kembali</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <table width="100%" class="table table-borderless">
                                <tr>
                                    <td width="38%" class="font-weight-bold">Nomor Invoice</td>
                                    <td width="2%" class="font-weight-bold">:</td>
                                    <td width="60%" class="font-weight-bold">{{$invoice->invoices_number}}</td>
                                </tr>
                                <tr>
                                    <td width="38%">Kasir</td>
                                    <td width="2%">:</td>
                                    <td width="60%">{{$invoice->user->name}}</td>
                                </tr>
                                <tr>
                                    <td width="38%">Tanggal Pembelian</td>
                                    <td width="2%">:</td>
                                    <td width="60%">{{$invoice->created_at}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table width="100%" class="table table-borderless">
                                <tr>
                                    <td width="38%">Jumlah Yang Dibayar</td>
                                    <td width="2%">:</td>
                                    <td width="60%">{{$invoice->pay}}</td>
                                </tr>
                                <tr>
                                    <td width="38%">Total Harga Obat</td>
                                    <td width="2%">:</td>
                                    <td width="60%">{{$invoice->total}}</td>
                                </tr>
                                <tr>
                                    <td width="38%">PPN</td>
                                    <td width="2%">:</td>
                                    <td width="60%">{{$invoice->tax}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-striped table-sm" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Medicine</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($invoice->purchaseItem as $index=>$item)
                                    <tr>
                                        <td>{{$index+1}}</td>
                                        <td>{{$item->medicine->name}}</td>
                                        <td>{{$item->stock}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection