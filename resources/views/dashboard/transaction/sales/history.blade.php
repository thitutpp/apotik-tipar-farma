@extends('layouts.pos')
@section('title', 'Riwayat Transaksi')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Riwayat Transaksi</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('sales.index') }}" class="text-muted">Kasir</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Riwayat Transaksi</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-lg-5 text-right">
            <a class="btn btn-xs btn-light" href="{{ route('home.index') }}"><i class="fas fa-hospital"></i><span class="hide-menu"> Dashboard</span></a>
            <a class="btn btn-xs btn-light" href="{{ route('sales.index') }}"><i class="fas fa-cart-arrow-down"></i><span class="hide-menu"> Kasir</span></a>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">Rp {{ number_format($total_today, 2) }}</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pendapatan Penjualan Hari Ini</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i data-feather="calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">Rp {{ number_format($total_this_month, 2) }}</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pendapatan Penjualan Selama Bulan Ini</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i data-feather="calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">Rp {{ number_format($total_this_year, 2) }}</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pendapatan Penjualan Selama Tahun Ini</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i data-feather="calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card" style="min-height: 85vh">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h4 class="font-weight-bold">Daftar Riwayat Transaksi</h4>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="history" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>Nomor Invoice</th>
                                    <th>Kasir</th>
                                    <th>Bayar</th>
                                    <th>Pajak</th>
                                    <th>Total</th>
                                    <th>Tanggal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#history').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('sales.history') }}",
            columns: [{
                    data: 'invoices_number'
                },
                {
                    data: 'user',
                    name: 'user.name'
                },
                {
                    data: 'pay'
                },
                {
                    data: 'tax'
                },
                {
                    data: 'total'
                },
                {
                    data: 'created_at'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    className: 'text-xl-center'
                },
            ]
        });
    });
</script>
@endpush