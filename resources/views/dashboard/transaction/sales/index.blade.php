@extends('layouts.pos')
@section('title', 'Kasir')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Kasir</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index', ) }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Kasir</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-lg-5 text-right">
            <a class="btn btn-xs btn-light" href="{{ route('home.index') }}"><i class="fas fa-hospital"></i><span class="hide-menu"> Dashboard</span></a>
            <a class="btn btn-xs btn-light" href="{{ route('sales.history') }}"><i class="fas fa-dollar-sign"></i><span class="hide-menu"> Riwayat Transaksi</span></a>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="bg-white card-header">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4 class="font-weight-bold">Data Obat</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="medicine-table" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>
                                        Tindakan
                                    </th>
                                    <th>
                                        Kode Obat
                                    </th>
                                    <th>
                                        Perusahaan Pemasok Obat
                                    </th>
                                    <th>
                                        Nama Obat
                                    </th>
                                    <th>
                                        Harga
                                    </th>
                                    <th>
                                        Stok
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Keranjang Belanja -->
        <div class="col-md-5    ">
            <div class="card">
                <div class="bg-white card-header">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4 class="font-weight-bold">Keranjang Belanja</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th width="10%">No</th>
                                    <th width="30%">Nama Obat</th>
                                    <th width="30%">Jumlah</th>
                                    <th width="30%" class="text-right">Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no=1
                                @endphp
                                @forelse($cart_data as $index=>$item)
                                <tr>
                                    <td>
                                        <form action="{{url('dashboard/sales/removemedicine',$item['rowId'])}}" method="POST">
                                            @csrf
                                            {{$no++ ?? ''}} <br><a onclick="this.closest('form').submit();return false;"><i class="fas fa-trash" style="color: rgb(134, 134, 134)"></i></a>
                                        </form>
                                    </td>
                                    <td>{{Str::words($item['name'],3)}} <br>Rp.
                                        {{ number_format($item['pricesingle'],2,',','.') ?? ''}}
                                    </td>
                                    <td class="font-weight-bold">

                                        <form action="{{url('dashboard/sales/decreasecart', $item['rowId'])}}" method="POST" style='display:inline;'>
                                            @csrf
                                            <button class="btn btn-sm btn-info"><i class="fas fa-minus"></i></button>
                                        </form>
                                        <a style="display: inline">{{$item['stock'] ?? ''}}</a>
                                        <form action="{{url('dashboard/sales/increasecart', $item['rowId'])}}" method="POST" style='display:inline;'>
                                            @csrf
                                            <button class="btn btn-sm btn-primary"><i class="fas fa-plus"></i></button>
                                        </form>
                                    </td>
                                    <td class="text-right">Rp. {{ number_format($item['price'],2,',','.') ?? ''}}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4" class="text-center">Keranjang Kosong</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <table class="table table-sm table-borderless">
                        <tr>
                            <th width="60%">Sub Total</th>
                            <th width="40%" class="text-right">Rp.
                                {{ number_format($data_total['sub_total'],2,',','.') ?? ''}}
                            </th>
                        </tr>
                        <tr>
                            <th>
                                <form action="{{ url('dashboard/sales') }}" method="get">
                                    PPN 10%
                                    <!-- <input type="checkbox" {{ $data_total['tax'] > 0 ? "checked" : ""}} name="tax" value="true" onclick="this.form.submit()"> -->
                                </form>
                            </th>
                            <th class="text-right">Rp.
                                {{ number_format($data_total['tax'],2,',','.') ?? ''}}
                            </th>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <th class="text-right font-weight-bold">Rp.
                                {{ number_format($data_total['total'],2,',','.') ?? ''}}
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row" style="float: right;">
                <form class="col-lg-6" action="{{ url('dashboard/sales/clear') }}" method="POST">
                    @csrf
                    <button class="btn btn-xs btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus keranjang ?');" type="submit">Bersihkan Keranjang</button>
                </form>
                <button class="btn btn-xs btn-success" data-toggle="modal" data-target="#fullHeightModal">Bayar</button>
            </div>
        </div>
    </div>
</div>
<div id="fullHeightModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="info-header-modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-colored-header bg-info">
                <h4 class="modal-title" id="info-header-modalLabel">Total Pembayaran</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-borderless">
                    <tr>
                        <th width="60%">Sub Total</th>
                        <th width="40%" class="text-right">Rp.
                            {{ number_format($data_total['sub_total'],2,',','.') ?? ''}}
                        </th>
                    </tr>
                    @if($data_total['tax'] > 0)
                    <tr>
                        <th>PPN 10%</th>
                        <th class="text-right">Rp.
                            {{ number_format($data_total['tax'],2,',','.') ?? ''}}
                        </th>
                    </tr>
                    @endif
                </table>
                <form action="{{ url('dashboard/sales/bayar') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="oke">Input Nominal</label>
                        <input id="oke" class="form-control" type="number" name="bayar" autofocus />
                    </div>
                    <h3 class="font-weight-bold">Total:</h3>
                    <h1 class="mb-5 font-weight-bold">Rp. {{ number_format($data_total['total'],2,',','.') }}</h1>
                    <input id="totalHidden" type="hidden" name="totalHidden" value="{{$data_total['total']}}" />

                    <h3 class="font-weight-bold">Bayar:</h3>
                    <h1 class="mb-5 font-weight-bold" id="pembayaran"></h1>

                    <h3 class="font-weight-bold text-primary">Kembalian:</h3>
                    <h1 class="font-weight-bold text-primary" id="kembalian"></h1>
            </div>

            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-success" id="saveButton" disabled onClick="openWindowReload(this)">Simpan Pembayaran</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{url('/dashboard/dist/js/pages/toastr/toastr.min.js')}}"></script>
@if(Session::has('error'))
<script>
    toastr.error(
        'Telah mencapai jumlah maksimal obat | Silahkan tambah stok obat terlebih dahulu untuk menambahkan'
    )
</script>
@endif

@if(Session::has('errorTransaksi'))
<script>
    toastr.error(
        'Transaksi tidak valid | Perhatikan jumlah pembayaran | Cek stok obat'
    )
</script>
@endif

@if(Session::has('success'))
<script>
    toastr.success(
        'Transaksi berhasil | Silahkan cek invoice anda'
    )
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#medicine-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('sales.index') }}",
            columns: [{
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    className: 'text-xl-center'
                },
                {
                    data: 'code'
                },
                {
                    data: 'supplier',
                    name: 'supplier.name'
                },
                {
                    data: 'name'
                },
                {
                    data: 'price'
                },
                {
                    data: 'stock'
                }
            ]
        });
    });
</script>

<script>
    $(document).ready(function() {
        $('#fullHeightModal').on('shown.bs.modal', function() {
            $('#oke').trigger('focus');
        });
    });

    oke.oninput = function() {
        let jumlah = parseInt(document.getElementById('totalHidden').value) ? parseInt(document.getElementById('totalHidden').value) : 0;
        let bayar = parseInt(document.getElementById('oke').value) ? parseInt(document.getElementById('oke').value) : 0;
        let hasil = bayar - jumlah;
        document.getElementById("pembayaran").innerHTML = bayar ? 'Rp ' + rupiah(bayar) + ',00' : 'Rp ' + 0 +
            ',00';
        document.getElementById("kembalian").innerHTML = hasil ? 'Rp ' + rupiah(hasil) + ',00' : 'Rp ' + 0 +
            ',00';

        cek(bayar, jumlah);
        const saveButton = document.getElementById("saveButton");

        if (jumlah === 0) {
            saveButton.disabled = true;
        }

    };

    function cek(bayar, jumlah) {
        const saveButton = document.getElementById("saveButton");

        if (bayar < jumlah) {
            saveButton.disabled = true;
        } else {
            saveButton.disabled = false;
        }
    }

    function rupiah(bilangan) {
        var number_string = bilangan.toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    }
</script>

@endpush

@push('style')
<link href="{{url('/dashboard/dist/js/pages/toastr/toastr.min.css')}}" rel="stylesheet">
@endpush