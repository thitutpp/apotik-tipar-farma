@extends('layouts.dashboard')
@section('title', 'Tambah Permission Obat')
@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Tambah Permission</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('permissions.index')}}" class="text-muted">Permission</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Permission</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>



<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Form Tambah Permissions</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('permissions.store') }}">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Permissions baru">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="text-left">
                                <button type="submit" class="btn btn-success"> <i class="far fa-save"> Simpan</i></button>
                                <button type="reset" class="btn btn-danger"> <i class="far fa-window-close"> Reset</i></button>
                                <a class="btn btn-dark" href="{{ route('permissions.index') }}">
                                    <i class="far fa-arrow-alt-circle-left"> Back </i>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection