@extends('layouts.dashboard')
@section('title', 'Edit Permission Obat')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Edit Permission</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('permissions.index')}}" class="text-muted">Permission</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Edit Permission</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Form Edit Permission</h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('permissions.update', [$permissions->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ old('name', $permissions->name) }}" required>
                            @if($errors->has('name'))
                            <span class="help-block" role="alert">{{ $errors->first('name') }}</span>
                            @endif

                        </div>
                        <div class="form-actions">
                            <button class="btn btn-success" type="submit">
                                <i class="far fa-save"> Simpan</i>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <i class="far fa-window-close"> Reset</i>
                            </button>
                            <a class="btn btn-dark" href="{{ route('permissions.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Back </i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection