@extends('layouts.dashboard')
@section('title', 'Lihat Peranan')
@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Lihat Peranan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('roles.index')}}" class="text-muted">Peranan</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Lihat Peranan</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            @can('role-create')
                            <a class="btn btn-dark" href="{{ route('roles.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                            </a>
                            @endcan
                        </div>
                    </div>

                    <table width="100%" class="table table-borderless">
                        <tr>
                            <td width="10%">Nama</td>
                            <td width="2%">:</td>
                            <td width="60%" class="font-weight-bold">{{ $role->name ?? ''}}</td>
                        </tr>
                        <tr>
                            <td width="10%">Izin</td>
                            <td width="2%">:</td>
                            <td width="60%">
                                @if(!empty($role->getPermissionNames()))
                                @foreach($role->getPermissionNames() as $p)
                                <label class="badge badge-success">{{ $p ?? '' }}</label>
                                @endforeach
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection