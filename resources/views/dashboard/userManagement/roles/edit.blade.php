@extends('layouts.dashboard')
@section('title', 'Ubah Peranan')
@section('content')
<style>
.two-col-special { overflow: auto; margin: 0; padding: 0; } 
.two-col-special li { display: inline-block; width: 45%; margin: 0; padding: 0; vertical-align: top; /* In case multi-word categories form two lines */ }
</style>
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Ubah Peranan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('roles.index')}}" class="text-muted">Peranan</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Ubah Peranan</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Tambah Peranan</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('roles.update', [$role->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-body">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input class="form-control" type="text" name="name" value="{{ old('name', $role->name) }}">
                            </div>
                            <label for="name">Izin</label>
                            <div class="form-group">
                                <fieldset class="checkbox">
                                    <ul class="two-col-special">
                                        @foreach($permission as $value)
                                        <li>
                                        <label>
                                            {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                            {{$value->name}}
                                        </label>
                                        </li>
                                        @endforeach
                                    </ul> 
                                </fieldset>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="text-left">
                                <button type="submit" class="btn btn-success"> <i class="far fa-save"> Simpan</i></button>
                                <button type="reset" class="btn btn-danger"> <i class="far fa-window-close"> Setel Ulang</i></button>
                                <a class="btn btn-dark" href="{{ route('roles.index') }}">
                                    <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection