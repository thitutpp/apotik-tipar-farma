@extends('layouts.dashboard')
@section('title', 'Tambah Pengguna')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Tambah Pengguna</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('users.index')}}" class="text-muted">Pengguna</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Pengguna</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Tambah Pengguna</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('users.store')}}">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Nama</label>
                                        <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Kata Sandi</label>
                                        <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm-password">Konfirmasi Kata Sandi</label>
                                        <input type="password" class="form-control" name="confirm-password" id="confirm-password" >

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="roles">Peranan</label>
                                        <select class="form-control select2" name="roles" id="roles" value="{{ old('roles') }}" >
                                            @foreach($roles as $id => $roles)
                                            <option value="{{ $id }}">{{ $roles }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-actions mt-5">
                                        <button class="btn btn-success" type="submit">
                                            <i class="far fa-save"> Simpan</i>
                                        </button>
                                        <button type="reset" class="btn btn-danger">
                                            <i class="far fa-window-close"> Setel Ulang</i>
                                        </button>
                                        <a class="btn btn-dark" href="{{ route('users.index') }}">
                                            <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection