@extends('layouts.dashboard')
@section('title', 'Pengguna')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Pengguna</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Pengguna</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

@if(Session::has('success'))
<div class="page-breadcrumb">
    @include('layouts.flash-success',[ 'message'=> Session('success') ])
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Pengguna</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-success" href="{{ route('users.create') }}">
                                <i class="far fa-plus-square"> Tambah Pengguna</i>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Nama
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th class="text-center">
                                        Peranan
                                    </th>

                                    <th class="text-center">
                                        Tindakan
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($user as $key => $user)
                                <tr>
                                    <td>
                                        {{ ++$no ?? '' }}
                                    </td>
                                    <td>
                                        {{ $user->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $user->email ?? '' }}
                                    </td>
                                    <td class="text-center">
                                        @if(!empty($user->getRoleNames()))
                                        @foreach($user->getRoleNames() as $v)
                                        <label class="badge badge-success">{{ $v }}</label>
                                        @endforeach
                                        @endif
                                    </td>
                                    
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-primary" href="{{ route('users.edit',$user->id) }}"><i class="far fa-edit"> Ubah</i></a>
                                        <form action="{{ route('users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('Apakah anda yakin ?');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-danger"><i class="far fa-window-close"> Hapus</i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection