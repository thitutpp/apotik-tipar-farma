@extends('layouts.dashboard')
@section('title', 'Pengeluaran')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Pengeluaran</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Pengeluaran</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

@if(Session::has('success'))
<div class="page-breadcrumb">
    @include('layouts.flash-success',[ 'message'=> Session('success') ])
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Pengeluaran</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-success" href="{{ route('expenses.create') }}">
                                <i class="far fa-plus-square"> Tambah Pengeluaran</i>
                            </a>
                            <a class="btn btn-dark" href="{{ route('expense.trash') }}">
                                <i class="far fa-trash-alt"> Tong Sampah</i>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Penanggung Jawab
                                    </th>
                                    <th>
                                        Kategori Pengeluaran
                                    </th>
                                    <th>
                                        Tanggal
                                    </th>
                                    <th>
                                        Biaya (Rp)
                                    </th>
                                    <th>
                                        Deskripsi
                                    </th>
                                    <th class="text-center">
                                        Tindakan
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($expenses as $key => $expense)
                                <tr>
                                    <td>
                                        {{ ++$no ?? '' }}
                                    </td>
                                    <td>
                                        {{$expense->user->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $expense->expense_category->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $expense->entry_date->toDateString() ?? '' }}
                                    </td>
                                    <td>
                                    Rp {{ number_format($expense['amount'], 2) }}
                                    </td>
                                    <td>
                                        {{ $expense->description ?? '' }}
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-warning text-white" href="{{ route('expenses.show',$expense->id) }}"><i class="fas fa-eye"> Lihat</i></a>
                                        <a class="btn btn-xs btn-primary" href="{{ route('expenses.edit',$expense->id) }}"><i class="far fa-edit"> Ubah</i></a>
                                        <form action="{{ route('expenses.destroy', $expense->id) }}" method="POST" onsubmit="return confirm('Apakah anda yakin ?');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-danger"> <i class="far fa-window-close"> Pindah ke Tong Sampah</i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection