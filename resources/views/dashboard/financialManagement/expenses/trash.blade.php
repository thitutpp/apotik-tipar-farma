@extends('layouts.dashboard')
@section('title', 'Tong Sampah Pengeluaran')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Tong Sampah Pengeluaran</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('expenses.index')}}" class="text-muted">Pengeluaran</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tong Sampah Pengeluaran</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

@if(Session::has('success'))
<div class="page-breadcrumb">
    @include('layouts.flash-success',[ 'message'=> Session('success') ])
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sampah</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-dark" href="{{ route('expenses.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                            </a>
                        </div>
                    </div> 
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                <th>
                                        No
                                    </th>
                                    <th>
                                        Kategori Pengeluaran
                                    </th>
                                    <th>
                                        Tanggal
                                    </th>
                                    <th>
                                        Biaya
                                    </th>
                                    <th>
                                        Deskripsi
                                    </th>
                                    <th>
                                        Tindakan
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($expense as $expenses)
                                <tr>
                                    <td>
                                        {{ ++$no ?? '' }}
                                    </td>
                                    <td>
                                        {{ $expenses->expense_category->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $expenses->entry_date->toFormattedDateString() ?? ''}}
                                    </td>
                                    <td>
                                        {{ $expenses->amount ?? '' }}
                                    </td>
                                    <td>
                                        {{ $expenses->description ?? '' }}
                                    </td>

                                    <td>
                                        <a class="btn btn-xs btn-primary" href="{{ route('expense.restore', $expenses->id) }}">
                                            <i class="far fa-window-restore"> Pulihkan</i>
                                        </a>
                                        <a class="btn btn-xs btn-danger" href="{{ route('expense.permanentDelete', $expenses->id) }}">
                                            <i class="far fa-trash-alt"> Hapus Permanen</i>
                                        </a>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection