@extends('layouts.dashboard')
@section('title', 'Ubah Pengeluaran')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Ubah Pengeluaran</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('expenses.index')}}" class="text-muted">Pengeluaran</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Ubah Pengeluaran</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Edit Pengeluaran</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('expenses.update', $expense->id) }}">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="expense_category_id">Kategori Pengeluaran</label>
                            <select class="form-control select2" name="expense_category_id" id="expense_category_id">
                                @foreach($expense_categories as $id => $expense_category)
                                <option value="{{ $id }}" {{ (old('expense_category_id') ? old('expense_category_id') : $expense->expense_category->id ?? '') == $id ? 'selected' : '' }}>{{ $expense_category ?? ''  }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="required" for="entry_date">Tanggal</label>
                            <input class="form-control" type="date" name="entry_date" id="entry_date" value="{{ old('entry_date', $expense->entry_date->toDateString()) }}" >
                        </div>
                        <div class="form-group">
                            <label class="required" for="amount">Biaya</label>
                            <input class="form-control" type="number" name="amount" id="amount" value="{{ old('amount', $expense->amount) }}" placeholder="Contoh : 1000">
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <input class="form-control" type="text" name="description" id="description" value="{{ old('description', $expense->description) }}">
                        </div>
                        <div class="form-actions mt-5">
                                <button class="btn btn-success" type="submit">
                                    <i class="far fa-save"> Simpan</i>
                                </button>
                                <button type="reset" class="btn btn-danger">
                                    <i class="far fa-window-close"> Setel Ulang</i>
                                </button>
                                <a class="btn btn-dark" href="{{ route('expenses.index') }}">
                                    <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                                </a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection