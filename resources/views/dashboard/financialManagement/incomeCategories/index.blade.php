@extends('layouts.dashboard')
@section('title', 'Kategori Pendapatan')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Kategori Pendapatan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Kategori Pendapatan</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

@if(Session::has('success'))
<div class="page-breadcrumb">
    @include('layouts.flash-success',[ 'message'=> Session('success') ])
</div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Daftar Kategori Pendapatan</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-success" href="{{ route('income-categories.create') }}">
                                <i class="far fa-plus-square"> Tambah Kategori Pendapatan</i>
                            </a>
                            <a class="btn btn-dark" href="{{ route('income-category.trash') }}">
                                <i class="far fa-trash-alt"> Tong Sampah</i>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Nama
                                    </th>
                                    <th class="text-center">
                                        Tindakan
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($incomeCategories as $incomeCategory)
                                <tr>
                                    <td>
                                        {{ ++$no ?? ''}}
                                    </td>
                                    <td>
                                        {{ $incomeCategory->name ?? ''}}
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-xs btn-info" href="{{ route('income-categories.edit', $incomeCategory->id) }}">
                                            <i class="far fa-edit"> Ubah</i>
                                        </a>
                                        <form action="{{ route('income-categories.destroy', $incomeCategory->id) }}" method="POST" onsubmit="return confirm('Apakah anda yakin ?');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-danger"> <i class="far fa-window-close"> Pindah ke Tong Sampah</i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection