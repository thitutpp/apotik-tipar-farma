@extends('layouts.dashboard')
@section('title', 'Lihat Pendapatan')
@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Lihat Pendapatan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('incomes.index')}}" class="text-muted">Pendapatan</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Lihat Pendapatan</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                   
                            <a class="btn btn-dark" href="{{ route('incomes.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                            </a>
                 
                        </div>
                    </div>

                    <table width="100%" class="table table-borderless">
                        <tr>
                            <td width="20%">Kategori Pengeluaran</td>
                            <td width="2%">:</td>
                            <td width="60%" class="font-weight-bold">{{ $income->income_category->name ?? '' }}</td>
                        </tr>
                        <tr>
                            <td width="20%">Tanggal</td>
                            <td width="2%">:</td>
                            <td width="60%" class="font-weight-bold">{{ $income->entry_date->toFormattedDateString() ?? '' }}</td>
                        </tr>
                        <tr>
                            <td width="20%">Biaya</td>
                            <td width="2%">:</td>
                            <td width="60%" class="font-weight-bold">{{ $income->amount ?? '' }}</td>
                        </tr>
                        <tr>
                            <td width="20%">Deskripsi</td>
                            <td width="2%">:</td>
                            <td width="60%" class="font-weight-bold">{{ $income->description ?? '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection