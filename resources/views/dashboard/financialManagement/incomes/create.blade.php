@extends('layouts.dashboard')
@section('title', 'Tambah Pendapatan')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Tambah Pendapatan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('incomes.index')}}" class="text-muted">Pendapatan</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Pendapatan</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Tambah Pendapatan</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('incomes.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="income_category_id">Kategori Pendapatan</label>
                            <select class="form-control select2" name="income_category_id" id="income_category_id">
                                @foreach($income_categories as $id => $income_category)
                                <option value="{{ $id }}" {{ old('income_category_id') == $id ? 'selected' : '' }} required>{{ $income_category }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="required" for="entry_date">Tanggal</label>
                            <input type="date" class="form-control" value="{{ old('entry_date') }}" name="entry_date" id="entry_date">
                        </div>
                        <div class="form-group">
                            <label class="required" for="amount">Biaya</label>
                            <input class="form-control" type="number" name="amount" id="amount" value="{{ old('amount', '') }}" step="0.01" placeholder="Contoh : 1000">
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <input class="form-control" type="text" name="description" id="description" value="{{ old('description', '') }}">
                        </div>
                        <div class="form-actions mt-5">
                            <button class="btn btn-success" type="submit">
                                <i class="far fa-save"> Simpan</i>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <i class="far fa-window-close"> Setel Ulang</i>
                            </button>
                            <a class="btn btn-dark" href="{{ route('incomes.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection