@extends('layouts.dashboard')
@section('title', 'Tong Sampah Pendapatan')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Tong Sampah Pendapatan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('incomes.index')}}" class="text-muted">Pendapatan</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tong Sampah Pendapatan</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if(Session::has('success'))
                    @include('layouts.flash-success',[ 'message'=> Session('success') ])
                    @endif
                    <h4 class="card-title">Sampah</h4>
                    <div style="margin-bottom: 10px;" class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-dark" href="{{ route('incomes.index') }}">
                                <i class="far fa-arrow-alt-circle-left"> Kembali </i>
                            </a>
                        </div>
                    </div> 
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Kategori Pendapatan
                                    </th>
                                    <th>
                                        Tanggal
                                    </th>
                                    <th>
                                        Biaya
                                    </th>
                                    <th>
                                        Deskripsi
                                    </th>
                                    <th>
                                        Tindakan
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($incomes as $income)
                                <tr>
                                    <td>
                                        {{ ++$no ?? '' }}
                                    </td>
                                    <td>
                                        {{ $income->income_category->name ?? '' }}
                                    </td>
                                    <td>
                                        {{ $income->entry_date->toFormattedDateString() ?? ''}}
                                    </td>
                                    <td>
                                        {{ $income->amount ?? '' }}
                                    </td>
                                    <td>
                                        {{ $income->description ?? '' }}
                                    </td>

                                    <td>
                                        <a class="btn btn-xs btn-primary" href="{{ route('income.restore', $income->id) }}">
                                            <i class="far fa-window-restore"> Pulihkan</i>
                                        </a>
                                        <a class="btn btn-xs btn-danger" href="{{ route('income.permanentDelete', $income->id) }}">
                                            <i class="far fa-trash-alt"> Hapus Permanen</i>
                                        </a>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection