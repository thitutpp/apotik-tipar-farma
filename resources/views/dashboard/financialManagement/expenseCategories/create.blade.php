@extends('layouts.dashboard')
@section('title', 'Tambah Kategori Pengeluaran')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Tambah Kategori Pengeluaran</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('expense-categories.index')}}" class="text-muted">Kategori Pengeluaran</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah Kategori Pengeluaran</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Formulir Tambah Kategori Pengeluaran</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Warning!</strong> There were some problems with your input.
                        <br><br>
                        <ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ route('expense-categories.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input class="form-control" type="text" name="name" id="name" placeholder="Nama Kategori Pengeluaran" value="{{ old('name', '') }}">
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="text-left">
                                <button class="btn btn-success" type="submit">
                                    <i class="far fa-save"> Simpan</i>
                                </button>
                                <button type="reset" class="btn btn-danger">
                                    <i class="far fa-window-close"> Setel Ulang</i>
                                </button>
                                <a class="btn btn-dark" href="{{ route('expense-categories.index') }}">
                                    <i class="far fa-arrow-alt-circle-left"> Kambali </i>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection