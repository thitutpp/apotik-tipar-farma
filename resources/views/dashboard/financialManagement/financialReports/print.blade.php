<html>

<head>
    <title>Print Laporan</title>

</head>

<style>
    .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        text-align: center;
    }
</style>

<body>
    <table cellspacing="0" border="0" width="100%">
        <tbody>
            <tr>
                <td  valign="middle"  align="left"><b>
                        <font size="3" color="black">APOTIK TIPAR FARMA </font>
                        <br><font size="1" color="black">Jl. Tipar Tengah No 10, Depok</font>
                    </b></td>
                <td  valign="middle" align="right">
                    <img src="{{public_path('/dashboard/assets/images/TiparFarma_logo.png')}}" > <img src="{{public_path('/dashboard/assets/images/TiparFarma_text.png')}}" >

                </td>
            </tr>
        </tbody>
    </table>
    <hr style="color: gray;">
    <p></p>
    <center>
        <h1>Laporan Keuangan </h1>
        <p>Tanggal {{$from}} - {{$to}} </p>
    </center>
    
    

    <!-- ********************************** Pendapatan **************************************** -->
    <table cellspacing="0" border="0" width="100%">
        <colgroup width="75"></colgroup>
        <colgroup width="176"></colgroup>
        <colgroup width="431"></colgroup>
        <tbody>
            <tr>
                <td colspan="3" valign="middle" bgcolor="#5f76e8" align="center"><b>
                        <font size="4" face="Times New Roman" color="#FFFFFF">Pendapatan</font>
                    </b></td>
            </tr>
            <tr>
                <td colspan="2" valign="middle" bgcolor="#F2F2F2" align="left"><b>
                        <font face="Times New Roman">Kategori</font>
                    </b></td>
                <td valign="middle" bgcolor="#F2F2F2" align="right"><b> 
                        <font face="Times New Roman"> Jumlah </font>
                    </b></td>
            </tr>
            @foreach($incomesSummary as $inc)
            <tr>
                <td colspan="2" valign="middle" align="left">
                    <font face="Times New Roman">{{ $inc['name'] }}</font>
                </td>
                <td valign="middle" align="right">
                    <font face="Times New Roman"> Rp {{ number_format($inc['amount'], 2) }} </font>
                </td>
            </tr>
            @endforeach

            <tr>
                <td style="border-top: 1px solid #000000" colspan="2" valign="middle" align="left"><b>
                        <font size="3" face="Times New Roman">Total Pendapatan</font>
                    </b></td>
                <td style="border-top: 1px solid #000000"valign="middle" align="right"><b>
                        <font size="3" face="Times New Roman"> Rp {{ number_format($incomesTotal, 2) }} </font>
                    </b></td>
            </tr>
            <tr>
                <td valign="middle" align="left">
                    <font face="Times New Roman"><br></font>
                </td>
                <td valign="middle" align="right"><i>
                        <font face="Times New Roman"><br></font>
                    </i></td>
                <td valign="middle" align="left">
                    <font face="Times New Roman"><br></font>
                </td>
            </tr>


        </tbody>
    </table>
    <!-- ********************************** Pendapatan **************************************** -->

    <!-- ********************************** Pengeluaran **************************************** -->

    <table cellspacing="0" border="0" width="100%">
        <colgroup width="75"></colgroup>
        <colgroup width="176"></colgroup>
        <colgroup width="431"></colgroup>
        <tbody>
            <tr>
                <td colspan="3" valign="middle" bgcolor="#5f76e8" align="center"><b>
                        <font size="4" face="Times New Roman" color="#FFFFFF">Pengeluaran</font>
                    </b></td>
            </tr>
            <tr>
                <td colspan="2" valign="middle" bgcolor="#F2F2F2" align="left"><b>
                        <font face="Times New Roman">Kategori</font>
                    </b></td>
                <td valign="middle" bgcolor="#F2F2F2" align="right"><b>
                        <font face="Times New Roman"> Jumlah </font>
                    </b></td>
            </tr>
            @foreach($expensesSummary as $inc)
            <tr>
                <td colspan="2" valign="middle" align="left">
                    <font face="Times New Roman">{{ $inc['name'] }}</font>
                </td>
                <td valign="middle" align="right">
                    <font face="Times New Roman"> Rp {{ number_format($inc['amount'], 2) }} </font>
                </td>
            </tr>
            @endforeach

            <tr>
                <td style="border-top: 1px solid #000000" colspan="2" valign="middle" align="left"><b>
                        <font size="3" face="Times New Roman">Total Pengeluaran</font>
                    </b></td>
                <td style="border-top: 1px solid #000000"  valign="middle" align="right"><b>
                        <font size="3" face="Times New Roman"> Rp {{ number_format($expensesTotal, 2) }} </font>
                    </b></td>
            </tr>
            <tr>
                <td valign="middle" align="left">
                    <font face="Times New Roman"><br></font>
                </td>
                <td valign="middle" align="right"><i>
                        <font face="Times New Roman"><br></font>
                    </i></td>
                <td valign="middle" align="left">
                    <font face="Times New Roman"><br></font>
                </td>
            </tr>


        </tbody>
    </table>
    <!-- ********************************** Pengeluaran **************************************** -->



    <!-- ********************************** Profit **************************************** -->
    <table cellspacing="0" border="0" width="100%">
        <tbody>
            <tr>
                <td colspan="3" valign="middle" bgcolor="#22ca80" align="center"><b>
                        <font size="4" face="Times New Roman" color="#FFFFFF">Profit</font>
                    </b></td>
            </tr>
            <tr>
                <td colspan="2" valign="middle" align="left">
                    <font size="3" face="Times New Roman">Total Pendapatan</font>
                </td>
                <td valign="middle" align="right">
                    <font face="Times New Roman"> Rp {{ number_format($incomesTotal, 2) }} </font>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="middle" align="left">
                    <font size="3" face="Times New Roman">Total Pengeluaran</font>
                </td>
                <td valign="middle" align="right">
                    <font face="Times New Roman"> Rp {{ number_format($expensesTotal, 2) }} </font>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid #000000" colspan="2" valign="middle" align="left"><b>
                        <font face="Times New Roman">Profit</font>
                    </b></td>
                <td style="border-top: 1px solid #000000" valign="middle" align="right"><b>
                        <font face="Times New Roman"> Rp {{ number_format($profit, 2) }}</font>
                    </b></td>
            </tr>
        </tbody>
    </table>
    <!-- ********************************** Profit **************************************** -->

    <div class="footer" style="padding-top: 10px; border-top: 1px solid #ddd; font-size:13px; color: #333; margin-top: 20px; text-align: center;">
    <font size="3" color="black">APOTIK TIPAR FARMA </font>
                        <br><font size="1" color="black">Jl. Tipar Tengah No 10, Depok</font>
    </div>
</body>

</html>