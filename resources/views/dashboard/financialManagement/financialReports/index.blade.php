@extends('layouts.dashboard')
@section('title', 'Laporan Keuangan')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="mb-1 page-title text-truncate text-dark font-weight-medium">Laporan Keuangan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="p-0 m-0 breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index')}}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Laporan Keuangan</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col col-6">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Telusuri Berdasarkan Jangka Waktu</h3>
                    <form method="GET" action="{{ url('dashboard/financial-reports/search') }}">
                        @csrf
                        <div class="form-group">
                            <label class="required" for="to">Tentukan Jangka Waktu</label>
                            <input type="text" class="form-control" name="daterange" value="">
                        </div>
                        <div class="form-actions mt-5">
                            <button class="btn btn-success" type="submit">
                                <i class="fab fa-sistrix"> Cari</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(isset($expensesSummary))
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Laporan</h3>
                    <div class="row">

                        <div class="col">
                            <h6>Profit</h6>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <th> Pendapatan</th>
                                        <td>Rp {{ number_format($incomesTotal, 2) ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <th>Pengeluaran</th>
                                        <td>Rp {{ number_format($expensesTotal, 2) ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <th>Profit</th>
                                        <td>Rp {{ number_format($profit, 2) ?? '' }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="col">
                            <h6>Daftar Kategori Pendapatan</h6>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <tr>

                                        <th>Kategori</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    @foreach($incomesSummary as $inc)
                                    <tr>

                                        <td>{{ $inc['name'] }}</td>
                                        <td>Rp {{ number_format($inc['amount'], 2) ?? '' }}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <th>Total Pendapatan</th>
                                        <th>Rp {{ number_format($incomesTotal, 2) ?? '' }}</th>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="col">
                            <h6>Daftar Kategori Pengeluaran</h6>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <tr>

                                        <th>Kategori</th>
                                        <th>Jumlah</th>
                                    </tr>
                                    @foreach($expensesSummary as $inc)
                                    <tr>

                                        <td>{{ $inc['name'] }}</td>
                                        <td>Rp {{ number_format($inc['amount'], 2) ?? ''}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <th>Total Pengeluaran</th>
                                        <th>Rp {{ number_format($expensesTotal, 2) ?? ''}}</th>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                    <a href="{{ url('dashboard/financial-reports/print_pdf/'.$from.'/'.$to)}}" class="btn btn-primary" target="_blank">CETAK PDF</a>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>

@endsection
@push('scripts')
<script>
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'right'
  }, function(start, end, label) {
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});
</script>
@endpush