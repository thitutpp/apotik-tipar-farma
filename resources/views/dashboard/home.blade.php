@extends('layouts.dashboard')
@section('title', 'Dashboard')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Selamat Datang {{ Auth::user()->name }} !</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item text-muted active" aria-current="page">Beranda</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <!-- *************************************************************** -->
    <!-- Start First Cards -->
    <!-- *************************************************************** -->
    <div class="card-group">
        <div class="card border-right">
            <div class="card-body bg-primary">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-light mb-1 font-weight-medium">{{$medicines}}</h2>
                            <span class="badge bg-cyan font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">Obat</span>
                        </div>
                        <h6 class="text-white font-weight-normal mb-0 w-100 text-truncate">Jumlah Obat</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body bg-secondary">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-white mb-1 font-weight-medium">{{$units}}</h2>
                            <span class="badge bg-cyan font-11 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">Satuan</span>
                        </div>
                        <h6 class="text-white font-weight-normal mb-0 w-100 text-truncate">Jumlah Satuan Obat</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body bg-danger">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-white mb-1 font-weight-medium">{{$categories}}</h2>
                            <span class="badge bg-cyan font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">Kategori</span>
                        </div>
                        <h6 class="text-white font-weight-normal mb-0 w-100 text-truncate">Jumlah Kategori Obat</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body bg-warning">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-white mb-1 font-weight-medium">{{$return}}</h2>
                            <span class="badge bg-cyan font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">Retur</span>
                        </div>
                        <h6 class="text-white font-weight-normal mb-0 w-100 text-truncate">Jumlah Retur</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body bg-success">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-white mb-1 font-weight-medium">{{$purchaseitem}}</h2>
                            <span class="badge bg-cyan font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">Transaksi</span>
                        </div>
                        <h6 class="text-white font-weight-normal mb-0 w-100 text-truncate">Transaksi</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">Rp {{ number_format($total_today, 2) }}</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pendapatan Penjualan Hari Ini</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i data-feather="calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">Rp {{ number_format($total_this_month, 2) }}</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pendapatan Penjualan Selama Bulan Ini</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i data-feather="calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">Rp {{ number_format($total_this_year, 2) }}</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Pendapatan Penjualan Selama Tahun Ini</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i data-feather="calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $chart1->options['chart_title'] }}</h4>
                    {!! $chart1->renderHtml() !!}
                    <ul class="list-inline text-center mt-5 mb-2">
                        <li class="list-inline-item text-muted font-italic">Pendapatan Keseluruhan Bulan Ini Rp {{ number_format($total_this_month1, 2) }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $chart2->options['chart_title'] }}</h4>
                    {!! $chart2->renderHtml() !!}
                    <ul class="list-inline text-center mt-5 mb-2">
                        <li class="list-inline-item text-muted font-italic">Pengeluaran Keseluruhan Bulan Ini Rp {{ number_format($total_this_month2, 2) }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
{!! $chart1->renderChartJsLibrary() !!}
{!! $chart1->renderJs() !!}
{!! $chart2->renderJs() !!}
@endpush