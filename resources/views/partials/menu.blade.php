<aside class="left-sidebar" data-sidebarbg="skin6">
        <div class="scroll-sidebar" data-sidebarbg="skin6">
                <nav class="sidebar-nav">
                        <ul id="sidebarnav">
                                @can('Akses Dashboard')
                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('home.index') }}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-hospital"></i> Beranda</h6>
                                        </a> 
                                </li>
                                @endcan
                                @can('Akses Manajemen Produk')
                                <li class="list-divider"></li>
                                <li class="nav-small-cap"><span class="hide-menu">Manajemen Produk</span></li>
                                                <li class="sidebar-item">
                                                        <a class="sidebar-link" href="{{ route('medicines.index') }}" aria-expanded="false">
                                                                <h6 class="hide-menu"><i class="fas fa-pills"></i>Obat</h6>
                                                        </a>
                                                </li>
                                                <li class="sidebar-item">
                                                        <a class="sidebar-link" href="{{ route('medicine-suppliers.index') }}" aria-expanded="false">
                                                                <h6 class="hide-menu"><i class="fas fa-parachute-box"></i>Pemasok Obat </h6>
                                                        </a>
                                                </li>
                                                <li class="sidebar-item">
                                                        <a class="sidebar-link" href="{{ route('medicine-categories.index') }}" aria-expanded="false">
                                                                <h6 class="hide-menu"><i class="fas fa-clipboard-list"></i>Kategori Obat</h6>
                                                        </a>
                                                </li>
                                                <li class="sidebar-item">
                                                        <a class="sidebar-link" href="{{ route('medicine-units.index') }}" aria-expanded="false">
                                                                <h6 class="hide-menu"><i class="fas fa-prescription-bottle"></i>Satuan Obat</h6>
                                                        </a>
                                                </li>
                                                @endcan
                                @can('Akses Manajemen Retur')
                                <li class="list-divider"></li>                                 
                                <li class="nav-small-cap"><span class="hide-menu">Manajemen Retur</span></li>
                                <li class="sidebar-item">
                                        <li class="sidebar-item">
                                                <a class="sidebar-link" href="{{ route('medicine-return.index')}}" aria-expanded="false">
                                                        <h6 class="hide-menu"><i class="fas fa-undo"></i>Daftar Retur Obat</h6>
                                                </a>
                                        </li>
                                        <li class="sidebar-item">
                                                <a class="sidebar-link" href="{{ route('medicine-return-categories.index')}}" aria-expanded="false">
                                                        <h6 class="hide-menu"><i class="fas fa-clipboard-list"></i>Kategori Retur Obat</h6>
                                                </a>
                                        </li>
                                        <li class="sidebar-item">
                                                <a class="sidebar-link" href="{{ url('dashboard/medicine-return-report/index')}}" aria-expanded="false">
                                                        <h6 class="hide-menu"><i class="fas fa-chart-line"></i>Laporan Retur Obat</h6>
                                                </a>
                                        </li>
                                        @endcan
                                </li>
                                @can('Akses Manajemen Transaksi')
                                <li class="list-divider"></li>
                                <li class="nav-small-cap"><span class="hide-menu">TRANSAKSI</span></li>
                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('sales.index') }}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-cart-arrow-down"></i> Kasir</h6>
                                        </a>
                                </li>
                                
                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('sales.history') }}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-dollar-sign"></i> Riwayat Transaksi</h6>
                                        </a>
                                </li>
                                @endcan

                                @can('Akses Manajemen Biaya')
                                <li class="list-divider"></li>
                                <li class="nav-small-cap"><span class="hide-menu">Manajemen biaya</span></li>
                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('expense-categories.index')}}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-clipboard-list"></i>Kategori Pengeluaran</h6>
                                        </a>
                                </li>
                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('income-categories.index')}}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-clipboard-list"></i>Kategori Pendapatan</h6>
                                        </a>
                                </li>
                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('expenses.index')}}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-minus-square"></i> Pengeluaran</h6>
                                        </a>
                                </li>
                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('incomes.index')}}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-plus-square"></i> Pendapatan</h6>
                                        </a>
                                </li>
                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('financial-reports.index')}}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-chart-line"></i> Laporan Keuangan</h6>
                                        </a>
                                </li>
                                @endcan


                                @can('Akses Manajemen Pengguna')
                                <li class="list-divider"></li>
                                <li class="nav-small-cap"><span class="hide-menu"> Manajemen Pengguna</span></li>

                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('roles.index') }}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-briefcase"></i> Peranan</h6>
                                        </a>
                                </li>

                                <li class="sidebar-item">
                                        <a class="sidebar-link" href="{{ route('users.index') }}" aria-expanded="false">
                                                <h6 class="hide-menu"><i class="fas fa-user"></i> Pengguna </h6>
                                        </a>
                                </li>
                                <li class="list-divider"></li>
                                @endcan
                        </ul>
                </nav>
        </div>
</aside>