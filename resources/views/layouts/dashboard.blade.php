<!DOCTYPE html>
<html>

<head>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/dashboard/assets/images/favicon.png')}}">
    <title>@yield('title')</title>
    <!-- This page plugin CSS -->
    <link type="text/css" href="{{url('/dashboard/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link type="text/css" href="{{url('/dashboard/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{url('/dashboard/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{url('/dashboard/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    <link type="text/css" href="{{ url('dashboard/dist/css/style.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{url('/dashboard/dist/css/icons/font-awesome/css/fontawesome.min.css')}}" rel="stylesheet">
    <!-- style date picker -->
    <link type="text/css" href="{{url('/dashboard/assets/extra-libs/datepicker/daterangepicker.css')}}" rel="stylesheet">
    @stack('style')
</head>

<body>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">

        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <div class="navbar-brand">
                        <a href="{{ route('home.index') }}">
                            <b class="logo-icon">
                                <img src="{{url('/dashboard/assets/images/TiparFarma_logo.png')}}" alt="homepage" class="dark-logo" />
                            </b>

                            <span class="logo-text">
                                <img src="{{url('/dashboard/assets/images/TiparFarma_text.png')}}" alt="homepage" class="dark-logo" />
                            </span>
                        </a>
                    </div>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="float-left pl-1 ml-3 mr-auto navbar-nav">

                    </ul>

                    <ul class="float-right navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{url('/dashboard/assets/images/users/profile-pic.jpg')}}" alt="user" class="rounded-circle" width="40">
                                <span class="ml-2 d-none d-lg-inline-block"><span class="text-dark">{{ Auth::user()->name }}</span> <i data-feather="chevron-down" class="svg-icon"></i></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <div>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i data-feather="power" class="ml-1 mr-2 svg-icon"></i>Keluar
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        @include('partials.menu')

        <div class="page-wrapper ">

            @yield('content')

            <footer class="text-center footer text-muted">
                Apotik Tipar Farma
            </footer>

        </div>

    </div>
    <script type="text/javascript" src="{{url('/dashboard/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script type="text/javascript" src="{{url('/dashboard/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script type="text/javascript" src="{{url('/dashboard/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Menu sidebar -->
    <script type="text/javascript" src="{{url('/dashboard/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript" src="{{url('/dashboard/dist/js/custom.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <!--This page plugins -->
    <script type="text/javascript" src="{{url('/dashboard/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/assets/extra-libs/c3/c3.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/dist/js/pages/dashboards/dashboard1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/dist/js/feather.min.js')}}"></script>
    <!-- date picker -->
    <script type="text/javascript" src="{{url('/dashboard/dist/js/pages/datepicker/datepicker-jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/dist/js/pages/datepicker/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/dashboard/dist/js/pages/datepicker/daterangepicker.min.js')}}"></script>
    @stack('scripts')
</body>

</html>