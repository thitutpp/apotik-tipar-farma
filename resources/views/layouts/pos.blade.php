<!DOCTYPE html>
<html>

<head>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('dashboard/assets/images/favicon.png')}}">
    <title>@yield('title')</title>
    <!-- This page plugin CSS -->
    <link type="text/css" href="{{url('dashboard/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link type="text/css" href="{{url('dashboard/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{url('dashboard/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{url('dashboard/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    <link type="text/css" href="{{ url('dashboard/dist/css/style.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{url('dashboard/dist/css/icons/font-awesome/css/fontawesome.min.css')}}" rel="stylesheet">
    @stack('style')
</head>

<body>
    <div data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">



        <div class="page-wrapper ">

            @yield('content')

            <footer class="text-center footer text-muted">
                System Developed by Thitut and Anistya. Using Adminmart Tamplate
            </footer>

        </div>




    </div>
    <script type="text/javascript" src="{{url('dashboard/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script type="text/javascript" src="{{url('dashboard/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('dashboard/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script type="text/javascript" src="{{url('dashboard/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('dashboard/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Menu sidebar -->
    <script type="text/javascript" src="{{url('dashboard/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript" src="{{url('dashboard/dist/js/custom.min.js')}}"></script>
    <!--This page plugins -->
    <script type="text/javascript" src="{{url('dashboard/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('dashboard/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script type="text/javascript" src="{{url('dashboard/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{url('dashboard/assets/extra-libs/c3/c3.min.js')}}"></script>
    <script type="text/javascript" src="{{url('dashboard/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script type="text/javascript" src="{{url('dashboard/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script type="text/javascript" src="{{url('dashboard/dist/js/feather.min.js')}}"></script>
    @stack('scripts')
</body>

</html>