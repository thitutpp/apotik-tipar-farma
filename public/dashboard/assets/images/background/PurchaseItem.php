<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseItem extends Model
{
    protected $dates = [
        'created_at', 
        'updated_at',
    ];

    protected $table = 'purchase_items';

    protected $fillable = [
        'medicine_id',
        'invoices_number',
        'stock',
        'created_at',
        'updated_at',
    ];

    public function medicine(){
        return $this->belongsTo(Medicine::class);
    }
}
