<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/auth/login');
});

Auth::routes();

Route::group(['namespace' => 'Dashboard', 'prefix' => 'dashboard', 'middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home.index');

    // Route Medicine
    Route::resource('medicines', 'MedicineController');
    Route::get('medicine/delete/{id}', 'MedicineController@destroy')->name('medicine.delete');
    Route::get('medicine/trash', 'MedicineController@trash')->name('medicine.trash');
    Route::get('medicine/restore/{id}', 'MedicineController@restore')->name('medicine.restore');
    Route::get('medicine/permanentDelete/{id}', 'MedicineController@permanentDelete')->name('medicine.permanentDelete');

    // Route Medicine Suppliers 
    Route::resource('medicine-suppliers', 'MedicineSupplierController');
    Route::get('medicine-supplier/trash', 'MedicineSupplierController@trash')->name('medicine-supplier.trash');
    Route::get('medicine-supplier/restore/{id}', 'MedicineSupplierController@restore')->name('medicine-supplier.restore');
    Route::get('medicine-supplier/permanentDelete/{id}', 'MedicineSupplierController@permanentDelete')->name('medicine-supplier.permanentDelete');


    // Route Medicine Categories
    Route::resource('medicine-categories', 'MedicineCategoryController');
    Route::get('medicine-category/trash', 'MedicineCategoryController@trash')->name('medicine-category.trash');
    Route::get('medicine-category/restore/{id}', 'MedicineCategoryController@restore')->name('medicine-category.restore');
    Route::get('medicine-category/permanentDelete/{id}', 'MedicineCategoryController@permanentDelete')->name('medicine-category.permanentDelete');

    // Route Medicine Units
    Route::resource('medicine-units', 'MedicineUnitController');
    Route::get('medicine-unit/trash', 'MedicineUnitController@trash')->name('medicine-unit.trash');
    Route::get('medicine-unit/restore/{id}', 'MedicineUnitController@restore')->name('medicine-unit.restore');
    Route::get('medicine-unit/permanentDelete/{id}', 'MedicineUnitController@permanentDelete')->name('medicine-unit.permanentDelete');

    // Route Medicine Return Categories 
    Route::resource('medicine-return-categories', 'MedicineReturCategoryController');
    Route::get('medicine-return-category/trash', 'MedicineReturCategoryController@trash')->name('medicine-return-category.trash');
    Route::get('medicine-return-category/restore/{id}', 'MedicineReturCategoryController@restore')->name('medicine-return-category.restore');
    Route::get('medicine-return-category/permanentDelete/{id}', 'MedicineReturCategoryController@permanentDelete')->name('medicine-return-category.permanentDelete');

    // Route Medicine Retur
    Route::resource('medicine-return', 'MedicineReturController');
    Route::get('medicine-return/all-expired/{id}', 'MedicineReturController@allExpired')->name('medicine-return.all.expired');
    Route::get('medicine-return-check/', 'MedicineReturController@checkExpired')->name('medicine-return.check.expired');
    Route::get('medicine-return-rollback/{id}', 'MedicineReturController@update')->name('medicine-return.rollback');

    // Route medicine retur report
    Route::get('medicine-return-report/index', 'MedicineReturnReportController@index')->name('medicine-return-report.index');
    Route::get('medicine-return-report/search', 'MedicineReturnReportController@search')->name('medicine-return-report.search');
    Route::get('medicine-return-report/print_pdf/{from}/{to}', 'MedicineReturnReportController@print_pdf')->name('medicine-return-report.print_pdf');

    // Route Point Of Sales
    Route::get('/sales', 'PurchaseItemController@index')->name('sales.index');
    Route::post('/sales/addmedicine/{id}', 'PurchaseItemController@addMedicineCart')->name('sales.addmedicine');
    Route::post('/sales/removemedicine/{id}', 'PurchaseItemController@removeMedicineCart');
    Route::post('/sales/increasecart/{id}', 'PurchaseItemController@increasecart');
    Route::post('/sales/decreasecart/{id}', 'PurchaseItemController@decreasecart');
    Route::post('/sales/clear', 'PurchaseItemController@clear');
    Route::post('/sales/bayar', 'PurchaseItemController@bayar');
    Route::get('/sales/history', 'PurchaseItemController@history')->name('sales.history');
    Route::get('/sales/invoice/{id}', 'PurchaseItemController@invoice')->name('sales.invoice');

    // Route Kategori Pengeluaran
    Route::resource('expense-categories', 'ExpenseCategoryController');
    Route::get('expense-category/trash', 'ExpenseCategoryController@trash')->name('expense-category.trash');
    Route::get('expense-category/restore/{id}', 'ExpenseCategoryController@restore')->name('expense-category.restore');
    Route::get('expense-category/permanentDelete/{id}', 'ExpenseCategoryController@permanentDelete')->name('expense-category.permanentDelete');

    // Route Kategori Pendapatan
    Route::resource('income-categories', 'IncomeCategoryController');
    Route::get('income-category/trash', 'IncomeCategoryController@trash')->name('income-category.trash');
    Route::get('income-category/restore/{id}', 'IncomeCategoryController@restore')->name('income-category.restore');
    Route::get('income-category/permanentDelete/{id}', 'IncomeCategoryController@permanentDelete')->name('income-category.permanentDelete');

    // Route Pengeluaran
    Route::resource('expenses', 'ExpenseController');
    Route::get('expense/trash', 'ExpenseController@trash')->name('expense.trash');
    Route::get('expense/restore/{id}', 'ExpenseController@restore')->name('expense.restore');
    Route::get('expense/permanentDelete/{id}', 'ExpenseController@permanentDelete')->name('expense.permanentDelete');

    // Route Pengeluaran
    Route::resource('incomes', 'IncomeController');
    Route::get('income/trash', 'IncomeController@trash')->name('income.trash');
    Route::get('income/restore/{id}', 'IncomeController@restore')->name('income.restore');
    Route::get('income/permanentDelete/{id}', 'IncomeController@permanentDelete')->name('income.permanentDelete');

    // Route Laporan Profit
    Route::get('financial-reports', 'FinancialReportController@index')->name('financial-reports.index');
    Route::get('financial-reports/search', 'FinancialReportController@search')->name('financial-reports.search');
    Route::get('financial-reports/print_pdf/{from}/{to}', 'FinancialReportController@print_pdf')->name('financial-reports.print_pdf');

    // User Management
    // Route::resource('permissions', 'PermissionsController');
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
});
