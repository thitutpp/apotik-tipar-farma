<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicineUnit extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at', 
        'updated_at',
        'deleted_at',
    ];

    protected $table = 'medicine_units';
    
    protected $fillable = [ 
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
