<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicineSupplier extends Model
{
    use SoftDeletes;
    
    protected $dates = [
        'created_at', 
        'updated_at',
        'deleted_at',
    ];

    protected $table = 'medicine_suppliers';

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
