<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicineReturCategory extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at', 
        'updated_at',
        'deleted_at',
    ];

    protected $table = "medicine_retur_categories";
    
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

//     protected $appends = ['hashid'];        
//     
//     public function getHashidAttribute()
//     {
//         return Hashids::encode($this->attributes['id']);
    // }

}
