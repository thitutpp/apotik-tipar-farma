<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicineRetur extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at', 
        'updated_at',
        'deleted_at',
    ];

    protected $table = 'medicine_retur';

    protected $fillable = [
        'medicine_id',
        'retur_category_id',
        'user_id',
        'qty_return',
        'tipe',
        'created_at',
        'updated_at',
        'deleted_at',
        
    ];

    public function medicine()
    {
        return $this->belongsTo(Medicine::class, 'medicine_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'User_id');
    }

    public function retur_category()
    {
        return $this->belongsTo(MedicineReturCategory::class, 'retur_category_id');
    }
}
