<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    public static function adminPermissions()
    {
        return [

            //Dashboard Permission
            'Akses Dashboard',

            /* Manajemen Pengguna */
            'Akses Manajemen Pengguna',
            //User Permissions
            // 'Akses Daftar Pengguna',
            // 'Akses Tambah Pengguna',
            // 'Akses Edit Pengguna',
            // 'Akses Hapus Pengguna Secara Permanen',
            //Role Permissions
            // 'Akses Daftar Role role-list',
            // 'Akses Tambah Role role-create',
            // 'Akses Edit Role role-edit',
            // 'Akses Hapus Role Secara Permanen',
            //Permissions
            // 'permissions-list',
            // 'permissions-create',
            // 'permissions-edit',
            // 'permissions-delete',
            // 'permissions-trash',
            // 'permissions-restore',
            // 'permissions-permanentDelete',
            /* Manajemen Pengguna */
            
            /* Manajemen Produk */
            'Akses Manajemen Produk',
                //Medicine Permissions
                // 'medicine-list',
                // 'medicine-create',
                // 'medicine-edit',
                // 'medicine-delete',
                // 'medicine-trash',
                // 'medicine-restore',
                // 'medicine-permanentDelete',
                //Medicine Categories Permissions
                // 'medicine-categories-list',
                // 'medicine-categories-create',
                // 'medicine-categories-edit',
                // 'medicine-categories-delete',
                // 'medicine-categories-trash',
                // 'medicine-categories-restore',
                // 'medicine-categories-permanentDelete',
                //Medicine Units Permissions
                // 'medicine-units-list',
                // 'medicine-units-create',
                // 'medicine-units-edit',
                // 'medicine-units-delete',
                // 'medicine-units-trash',
                // 'medicine-units-restore',
                // 'medicine-units-permanentDelete',
                //Medicine Supplier Permissions
                // 'medicine-supplier-list',
                // 'medicine-supplier-create',
                // 'medicine-supplier-edit',
                // 'medicine-supplier-delete',
                // 'medicine-supplier-trash',
                // 'medicine-supplier-restore',
                // 'medicine-supplier-permanentDelete',
            /* Manajemen Produk */

            /* Manajemen Retur */
            'Akses Manajemen Retur',
                //Medicine Return Permissions
                // 'medicine-return-list',
                // 'medicine-return-create',
                // 'medicine-return-edit',
                // 'medicine-expired-return-action',
                //Medicine Return Categories Permissions
                // 'medicine-return-categories-list',
                // 'medicine-return-categories-create',
                // 'medicine-return-categories-edit',
                // 'medicine-return-categories-delete',
                // 'medicine-return-categories-trash',
                // 'medicine-return-categories-restore',
                // 'medicine-return-categories-permanentDelete',
                //Return Report Print Permissions
                // 'return-reports-list',
            /* Manajemen Retur */


            'Akses Manajemen Transaksi',
            // /* Transaksi */
            // 'Transaksi',
            //     //Kasir Permissions
            //     'sales-list',
            //     'sales-history',
            //     'sales-invoice',
            // /* Transaksi */

            /* Manajemen Biaya */
            'Akses Manajemen Biaya',
                //Categories Expense Permissions
                // 'categories-expense-list',
                // 'categories-expense-create',
                // 'categories-expense-edit',
                // 'categories-expense-delete',
                // 'categories-expense-trash',
                // 'categories-expense-restore',
                // 'categories-expense-permanentDelete',
                //Categories Income Permissions
                // 'categories-income-list',
                // 'categories-income-create',
                // 'categories-income-edit',
                // 'categories-income-delete',
                // 'categories-income-trash',
                // 'categories-income-restore',
                // 'categories-income-permanentDelete',
                //Expense Permissions
                // 'expense-list',
                // 'expense-create',
                // 'expense-edit',
                // 'expense-delete',
                // 'expense-trash',
                // 'expense-restore',
                // 'expense-permanentDelete',
                //Income Permissions
                // 'income-list',
                // 'income-create',
                // 'income-edit',
                // 'income-delete',
                // 'income-trash',
                // 'income-restore',
                // 'income-permanentDelete',
                //Financial Report Print Permissions
                // 'financial-reports-list',
            /* Manajemen Biaya */
        ];
    }

    public static function cashierPermissions()
    {
        return [

        //Dashboard Permission
        'Akses Dashboard',

        'Akses Manajemen Transaksi'
        
        // //Kasir Permissions
        // 'sales-list',
        // 'sales-history',
        // 'sales-invoice',

        ];
    }
}
