<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryItem extends Model
{
    protected $dates = [
        'created_at', 
        'updated_at',
    ];

    protected $table = 'history_items';

    protected $fillable = [
        'medicine_id',
        'user_id',
        'stock',
        'stockChange',
        'last_stock',
        'tipe',
        'created_at',
        'updated_at',
    ];

    public function user(){
        return $this->belongsTo(User::class);
}
}
