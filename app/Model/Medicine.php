<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicine extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at', 
        'updated_at',
        'deleted_at',
    ];

    protected $table = 'medicines';

    protected $fillable = [
        'code',
        'name',
        'weight',
        'hpp',
        'price',
        'stock',
        'expired',
        'is_expired',
        'unit_id',
        'category_id',
        'supplier_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function supplier()
    {
        return $this->belongsTo(MedicineSupplier::class, 'supplier_id', 'id');
    }

    public function unit()
    {
        return $this->belongsTo(MedicineUnit::class, 'unit_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(MedicineCategory::class, 'category_id');
    }
}
