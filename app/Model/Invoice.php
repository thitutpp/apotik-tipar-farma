<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $table = 'invoices';

    protected $fillable = [
        'invoices_number',
        'user_id',
        'pay',
        'tax',
        'total',
        'created_at',
        'updated_at',
    ];
    // protected $primaryKey = 'invoices_number';
    protected $keyType = 'string';

    public function purchaseItem()
    {
        return $this->hasMany(PurchaseItem::class, 'invoices_number', 'invoices_number');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
