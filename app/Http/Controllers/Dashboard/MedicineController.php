<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Medicine;
use App\Model\MedicineCategory;
use App\Model\MedicineUnit;
use App\Model\MedicineSupplier;
use App\Model\HistoryItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DataTables;
 
class MedicineController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Produk', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:medicine-list', ['only' => ['index']]);
    //     $this->middleware('permission:medicine-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:medicine-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:medicine-delete', ['only' => ['destroy']]);
    //     $this->middleware('permission:medicine-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:medicine-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:medicine-permanentDelete', ['only' => ['permanentDelete']]);
        
    // }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $medicine = Medicine::all();
            return DataTables::of($medicine)
                    ->addIndexColumn()
                    ->editColumn('price', function ($medicine) {
                        return "Rp ". number_format($medicine->price, 2) ?? "";
                    })
                    ->editColumn('hpp', function ($medicine) {
                        return "Rp ". number_format($medicine->hpp, 2) ?? "";
                    })
                    ->editColumn('supplier', function ($medicine) {
                        return $medicine->supplier->name ?? "";  
                    })
                    ->editColumn('unit', function ($medicine) {
                        return $medicine->unit->name ?? "";         
                    })
                    ->editColumn('category', function ($medicine) {
                        return $medicine->category->name ?? "";  
                    })
                    ->addColumn('action', function($medicine){
                        $btn ='';
                        // if (Auth::user()->roles->pluck('name')[0] == 'dashboard.) {
                            $btn =
                            '<a href="' . route('medicines.edit', $medicine->id) . '" class="edit btn btn-primary btn-sm"><i class="far fa-edit"> Ubah</i></a>
                            <button type="button" name="delete" id="' . $medicine->id . '" class="delete btn btn-danger btn-sm"><i class="far fa-window-close"> Pindah Ke Tong Sampah</i></button>';
                        //    }
                            return $btn; 
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('dashboard.productManagement.medicines.index');
    }

    public function create()
    {
        $suppliers = MedicineSupplier::all()->pluck('name', 'id')->prepend(trans('Pilih Perushaan'), '');

        $units = MedicineUnit::all()->pluck('name', 'id')->prepend(trans('Pilih Satuan'), '');

        $categories = MedicineCategory::all()->pluck('name', 'id')->prepend(trans('Pilih Kategori'), '');

        return view('dashboard.productManagement.medicines.create', compact('units', 'categories', 'suppliers'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->validate($request, [
                'unit_id',
                'category_id',
                'supplier_id',
                'code' => 'required|unique:medicines',
                'name' => 'required|unique:medicines',
                'weight' => 'required',
                'hpp' => 'required',
                'price' => 'required',
                'stock' => 'required',
                'expired' => 'required'
            ]);

            $medicine = Medicine::create([
                'unit_id' => $request->unit_id,
                'category_id' => $request->category_id,
                'supplier_id' => $request->supplier_id,
                'code' => $request->code,
                'name' => $request->name,
                'weight' => $request->weight,
                'hpp' => $request->hpp,
                'price' => $request->price,
                'stock' => $request->stock,
                'expired' => $request->expired,
                'is_expired' => FALSE
            ]);

            HistoryItem::create([
                'medicine_id' => $medicine->id,
                'user_id' => Auth::id(),
                'stock' => $request->stock,
                'stockChange' => 0,
                'last_stock' => 0,
                'tipe' => 'created product'
            ]);

            $message = 'Data obat berhasil ditambahkan';

            DB::commit();
            return redirect()->route('medicines.index')->with('success', $message);
        } catch (\Exeception $e) {
            DB::rollback();
            return redirect()->route('medicines.create')->with('error', $e);
        }
    }

    public function edit($id)
    {
        $medicine = Medicine::find($id);

        $history = HistoryItem::where('medicine_id',$id)->orderBy('created_at','desc')->get();

        $units = MedicineUnit::all()->pluck('name', 'id')->prepend(trans('Pilih Satuan'), '');

        $categories = MedicineCategory::all()->pluck('name', 'id')->prepend(trans('Pilih Kategori'), '');

        $suppliers = MedicineSupplier::all()->pluck('name', 'id')->prepend(trans('Pilih Pemasok'), '');
        
        $medicine->load('unit', 'category', 'supplier');

        return view('dashboard.productManagement.medicines.edit', compact('units', 'categories', 'medicine', 'history', 'suppliers'));
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $id = $request->id;
            $this->validate($request, [
                'unit_id' => '',
                'category_id' => '',
                'supplier_id' => '',
                'name' => 'required',
                'weight' => 'required',
                'hpp' => 'required',
                'price' => 'required',
                'stock' => 'required',
                'expired' => 'required',
            ]);

            if ($request->addStock) {
                $stock = $request->stock + $request->addStock;
                if ($stock < 0) {
                    return redirect()->back()->with('errorStock', 'Quantity cant be lower than zero');
                }
            } else {
                $stock = $request->stock;
            }

            $medicine_id = Medicine::find($id);
            $medicine = [
                'unit_id' => $request->unit_id,
                'category_id' => $request->category_id,
                'supplier_id' =>$request->supplier_id,
                'name' => $request->name,
                'weight' => $request->weight,
                'hpp' => $request->hpp,
                'price' => $request->price,
                'stock' => $stock,
                'expired' => $request->expired
            ];

            $medicine_id->update($medicine);
            if ($request->addStock) {
                HistoryItem::create([
                    'medicine_id' => $medicine_id->id,
                    'user_id' => Auth::id(),
                    'stock' => $request->stock,
                    'stockChange' => $request->addStock,
                    'last_stock' => ($medicine_id->stock + $request->stock),

                    'tipe' => 'change product stock from admin'
                ]);
            }

            $message = 'Data obat berhasil diperbaruhi';
            DB::commit();
            return redirect()->route('medicines.index')->with('success', $message);
        } catch (\Exeception $e) {
            DB::rollback();
            return redirect()->route('medicines.create')->with('error', $e);
        }
    }

    public function destroy($id)
    {
        $medicines = Medicine::findOrFail($id);
        $medicines->delete();
        $message="Data obat berhasil dihapus";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $medicines = Medicine::onlyTrashed()->get();
        return view('dashboard.productManagement.medicines.trash', compact('medicines'))->with('no');
    }

    public function restore($id)
    {
        $medicines = Medicine::onlyTrashed()->where('id', $id);
        $medicines->restore();
        $message="Data obat berhasil di pulihkan";
        return redirect()->route('medicines.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $medicines = Medicine::onlyTrashed()->where('id', $id);
        $medicines->forceDelete();
        $message="Data obat berhasil di hapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
