<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MedicineUnit;

class MedicineUnitController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Produk', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:medicine-units-list', ['only' => ['index']]);
    //     $this->middleware('permission:medicine-units-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:medicine-units-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:medicine-units-delete', ['only' => ['destroy']]); 
    //     $this->middleware('permission:medicine-units-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:medicine-units-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:medicine-units-permanentDelete', ['only' => ['permanentDelete']]);
    // }
    public function index()
    {
        $medicineUnits = MedicineUnit::all();
        return view('dashboard.productManagement.medicineUnits.index', compact('medicineUnits'))->with('no');
    }

    public function create()
    {
        return view('dashboard.productManagement.medicineUnits.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:medicine_units,name',
        ]);

        MedicineUnit::create($request->all());

        $message = 'Data satuan obat berhasil ditambahkan';
        return redirect()->route('medicine-units.index')->with('success', $message);
    }

    public function edit($id)
    {
        $medicineUnits = MedicineUnit::find($id);
        return view('dashboard.productManagement.medicineUnits.edit', compact('medicineUnits'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:medicine_units,name',

        ]);

        $medicineUnits = MedicineUnit::find($id);
        $medicineUnits->name = $request->input('name');
        $medicineUnits->save();
        $message = "Data satuan obat berhasil diperbaruhi";
        return redirect()->route('medicine-units.index')->with('success', $message);
    }

    public function destroy($id)
    {
        $medicineUnits = MedicineUnit::find($id);
        $medicineUnits->delete();
        $message = "Data satuan obat berhasil dihapus";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $medicineUnits = MedicineUnit::onlyTrashed()->get();
        return view('dashboard.productManagement.medicineUnits.trash', compact('medicineUnits'))->with('no');
    }

    public function restore($id)
    {
        $medicineUnits = MedicineUnit::onlyTrashed()->where('id', $id);
        $medicineUnits->restore();
        $message = "Data satuan obat berhasil dipulihkan";
        return redirect()->route('medicine-units.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $medicineUnits = MedicineUnit::onlyTrashed()->where('id', $id);
        $medicineUnits->forceDelete();
        $message = "Data satuan obat berhasil dihapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
