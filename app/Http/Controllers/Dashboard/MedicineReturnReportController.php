<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Model\MedicineRetur;
use PDF;
class MedicineReturnReportController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Retur', ['only' => ['index']]);
    }
    // function __construct()
    // {
    //     $this->middleware('permission:return-reports-list', ['only' => ['index']]);
    // }

    public function index(){
        return view('dashboard.productManagement.medicineReturnReports.index');
    }

    public function search(Request $request)
    {
        $this->validate($request,[
            'daterange'=>'required'
        ]);


        
        $dateRange = explode('-', str_replace(' ', '', $request->daterange));
        
        $from = date('Y-m-d', strtotime($dateRange[0])).' 00:00:00';
        $to = date('Y-m-d', strtotime($dateRange[1])).' 23:59:59';
    
        $returns = MedicineRetur::whereBetween('created_at', [$from,$to])->get();
   
        return view('dashboard.productManagement.medicineReturnReports.index', compact(
            'returns', 'from', 'to'
        ));
    }

    public function print_pdf($from, $to)
    { 
        
        $from2 = Carbon::createFromFormat('Y-m-d H:i:s', $from)->format('Y-m-d');
        $to2 = Carbon::createFromFormat('Y-m-d H:i:s', $to)->format('Y-m-d');
        $returns = MedicineRetur::with('retur_category')->whereBetween('created_at', [$from,$to])->get();
 
    	$pdf = PDF::loadview('dashboard.productManagement.medicineReturnReports.print',compact(
            'returns',          
            'from2',
            'to2'
        ));

    	return $pdf->download('Laporan-Keuangan-Apotik-Indonesia');
    }
}
