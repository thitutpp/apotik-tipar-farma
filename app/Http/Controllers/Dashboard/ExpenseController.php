<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\Expense;
use App\Model\ExpenseCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Biaya', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:expense-list', ['only' => ['index']]);
    //     $this->middleware('permission:expense-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:expense-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:expense-delete', ['only' => ['destroy']]);
    //     $this->middleware('permission:expense-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:expense-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:expense-permanentDelete', ['only' => ['permanentDelete']]);
    // }

    public function index()
    {
        $expenses = Expense::with(['expense_category'])->get();
        return view('dashboard.financialManagement.expenses.index', compact('expenses'))->with('no');
    }

    public function create()
    {
        $expense_categories = ExpenseCategory::all()->pluck('name', 'id')->prepend(trans('Pilih Kategori'), '');
        return view('dashboard.financialManagement.expenses.create', compact('expense_categories'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'expense_category_id' => 'required',
            'entry_date' => 'required',
            'amount' => 'required',
            'description' => '',

        ]);

        Expense::create([
            'user_id' => Auth::id(),
            'expense_category_id' => $request->expense_category_id,
            'entry_date' => $request->entry_date,
            'amount' => $request->amount,
            'description' => $request->description,
        ]);
        $message = 'Data pengeluaran berhasil ditambahkan';
        return redirect()->route('expenses.index')->with('success', $message);
    }


    public function show(Expense $expense)
    {
        $expense->load('expense_category');
        return view('dashboard.financialManagement.expenses.show', compact('expense'));
    }


    public function edit($id)
    {
        $expense = Expense::find($id);
        $expense_categories = ExpenseCategory::all()->pluck('name', 'id')->prepend(trans('Pilih Kategori'), '');
        $expense->load('expense_category');
        return view('dashboard.financialManagement.expenses.edit', compact('expense_categories', 'expense'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'expense_category_id' => '',
            'entry_date' => 'required',
            'amount' => 'required',
            'description' => '',

        ]);

        $expense  = Expense::find($id);
        $expense->expense_category_id = $request->input('expense_category_id');
        $expense->entry_date = $request->input('entry_date');
        $expense->amount = $request->input('amount');
        $expense->description = $request->input('description');
        $expense->save();
        $message = 'Data pengeluaran berhasil diperbaruhi';
        return redirect()->route('expenses.index')->with('success', $message);
    }


    public function destroy($id)
    {
        $expense = Expense::find($id);
        $expense->delete();
        $message = "Data pengeluaran berhasil dihapus";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $expense = Expense::onlyTrashed()->get();
        return view('dashboard.financialManagement.expenses.trash', compact('expense'))->with('no');
    }

    public function restore($id)
    {
        $expense = Expense::onlyTrashed()->where('id', $id);
        $expense->restore();
        $message = "Data pengeluaran berhasil dipulihkan";
        return redirect()->route('expenses.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $expense = Expense::onlyTrashed()->where('id', $id);
        $expense->forceDelete();
        $message = "Data pengeluaran berhasil dihapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
