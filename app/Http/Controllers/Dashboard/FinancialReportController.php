<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Expense;
use App\Model\Income;
use Carbon\Carbon;
use PDF;
use Excel;

class FinancialReportController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Biaya', ['only' => ['index']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:financial-reports-list', ['only' => ['index']]);
    // }

    public function index(){
        return view('dashboard.financialManagement.financialReports.index');
    }

    public function search(Request $request)
    {
        $this->validate($request,[
            'daterange'=>'required'
        ]);

        $dateRange = explode('-', $request->daterange);
        
        $from = date('Y-m-d', strtotime($dateRange[0]));
        $to = date('Y-m-d', strtotime($dateRange[1]));

        $expenses = Expense::with('expense_category')
            ->whereBetween('entry_date', [$from,$to]);

        $incomes = Income::with('income_category')
            ->whereBetween('entry_date', [$from,$to]);

        $expensesTotal   = $expenses->sum('amount');
        $incomesTotal    = $incomes->sum('amount');
        $groupedExpenses = $expenses->whereNotNull('expense_category_id')->orderBy('amount', 'desc')->get()->groupBy('expense_category_id');
        $groupedIncomes  = $incomes->whereNotNull('income_category_id')->orderBy('amount', 'desc')->get()->groupBy('income_category_id');
        $profit          = $incomesTotal - $expensesTotal;

        $expensesSummary = [];

        foreach ($groupedExpenses as $exp) {
            foreach ($exp as $line) {
                if (!isset($expensesSummary[$line->expense_category->name])) {
                    $expensesSummary[$line->expense_category->name] = [
                        'name'   => $line->expense_category->name,
                        'amount' => 0,
                    ];
                }

                $expensesSummary[$line->expense_category->name]['amount'] += $line->amount;
            }
        }

        $incomesSummary = [];

        foreach ($groupedIncomes as $inc) {
            foreach ($inc as $line) {
                if (!isset($incomesSummary[$line->income_category->name])) {
                    $incomesSummary[$line->income_category->name] = [
                        'name'   => $line->income_category->name,
                        'amount' => 0,
                    ];
                }

                $incomesSummary[$line->income_category->name]['amount'] += $line->amount;
            }
        }


        return view('dashboard.financialManagement.financialReports.index', compact(
            'expensesSummary',
            'incomesSummary',
            'expensesTotal',
            'incomesTotal',
            'profit', 
            'from',
            'to'
        ));
    }

    public function print_pdf($from, $to)
    { 
        $expenses = Expense::with('expense_category')
            ->whereBetween('entry_date', [$from,$to]);

        $incomes = Income::with('income_category')
            ->whereBetween('entry_date', [$from,$to]);

        $expensesTotal   = $expenses->sum('amount');
        $incomesTotal    = $incomes->sum('amount');
        $groupedExpenses = $expenses->whereNotNull('expense_category_id')->orderBy('amount', 'desc')->get()->groupBy('expense_category_id');
        $groupedIncomes  = $incomes->whereNotNull('income_category_id')->orderBy('amount', 'desc')->get()->groupBy('income_category_id');
        $profit          = $incomesTotal - $expensesTotal;

        $expensesSummary = [];

        foreach ($groupedExpenses as $exp) {
            foreach ($exp as $line) {
                if (!isset($expensesSummary[$line->expense_category->name])) {
                    $expensesSummary[$line->expense_category->name] = [
                        'name'   => $line->expense_category->name,
                        'amount' => 0,
                    ];
                }

                $expensesSummary[$line->expense_category->name]['amount'] += $line->amount;
            }
        }

        $incomesSummary = [];

        foreach ($groupedIncomes as $inc) {
            foreach ($inc as $line) {
                if (!isset($incomesSummary[$line->income_category->name])) {
                    $incomesSummary[$line->income_category->name] = [
                        'name'   => $line->income_category->name,
                        'amount' => 0,
                    ];
                }

                $incomesSummary[$line->income_category->name]['amount'] += $line->amount;
            }
        }
        
    	$pdf = PDF::loadview('dashboard.financialManagement.financialReports.print',compact(
            'expensesSummary',
            'incomesSummary',
            'expensesTotal',
            'incomesTotal',
            'profit',
            'from',
            'to'
        ));

    	return $pdf->download('Laporan-Keuangan-Apotik-Indonesia');
    }
}
