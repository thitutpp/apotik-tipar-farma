<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\MedicineReturCategory;
use Illuminate\Http\Request;

class MedicineReturCategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Produk', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:medicine-return-categories-list', ['only' => ['index']]);
    //     $this->middleware('permission:medicine-return-categories-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:medicine-return-categories-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:medicine-return-categories-delete', ['only' => ['destroy']]); 
    //     $this->middleware('permission:medicine-return-categories-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:medicine-return-categories-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:medicine-return-categories-permanentDelete', ['only' => ['permanentDelete']]);
    // }

    public function index()
    {
        $returCategories  = MedicineReturCategory::where('id', '!=', 1)->get();
        return view('dashboard.productManagement.returnCategories.index', compact('returCategories'))->with('no');
    }

    public function create()
    {
        return view('dashboard.productManagement.returnCategories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:medicine_retur_categories,name',
        ]);

        MedicineReturCategory::create([
            'name' => ucwords($request->input('name'))
        ]);

        $message = 'Data kategori return berhasil di ditambahkan';
        return redirect()->route('medicine-return-categories.index')->with('success', $message);
    }

    public function edit($id)
    {
        $returCategories  = MedicineReturCategory::find($id);
        return view('dashboard.productManagement.returnCategories.edit', compact('returCategories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',

        ]);

        $returCategories  = MedicineReturCategory::find($id);
        $returCategories->name = ucwords($request->input('name'));
        $returCategories->save();
        $message = 'Data kategori return berhasil di perbaruhi';
        return redirect()->route('medicine-return-categories.index')->with('success', $message);
    }

    public function destroy($id)
    {
        $returCategories  = MedicineReturCategory::find($id);
        $returCategories->delete();
        $message = "Data kategori return berhasil dipindah ke tong sampah";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $returCategories  = MedicineReturCategory::onlyTrashed()->get();
        return view('dashboard.productManagement.returnCategories.trash', compact('returCategories'))->with('no');
    }

    public function restore($id)
    {
        $returCategories  = MedicineReturCategory::onlyTrashed()->where('id', $id);
        $returCategories->restore();
        $message = "Data kategori return berhasil di pulihkan";
        return redirect()->route('medicine-return-categories.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $returCategories  = MedicineReturCategory::onlyTrashed()->where('id', $id);
        $returCategories->forceDelete();
        $message = "Data kategori return berhasil di hapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
