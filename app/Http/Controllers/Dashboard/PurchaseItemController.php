<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\HistoryItem;
use App\Model\Medicine;
use App\Model\PurchaseItem;
use App\Model\Invoice;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Darryldecode\Cart\CartCondition;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;
use Illuminate\Support\Carbon;
use DataTables;

use Haruncpi\LaravelIdGenerator\IdGenerator;
use SebastianBergmann\CodeCoverage\Report\Xml\Totals;

class PurchaseItemController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Transaksi', ['only' => ['index', 'history', 'invoice']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:sales-list', ['only' => ['index']]);
    //     $this->middleware('permission:sales-history', ['only' => ['history']]);
    //     $this->middleware('permission:sales-invoice', ['only' => ['invoice']]);
    // }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $medicine = Medicine::all();
            return DataTables::of($medicine)
                ->addIndexColumn()
                ->editColumn('code', function ($medicine) {
                    return $medicine->code ?? "";
                })
                ->editColumn('supplier', function ($medicine) {
                    return $medicine->supplier->name ?? "";
                })
                ->editColumn('name', function ($medicine) {
                    return $medicine->name ?? "";
                })
                ->editColumn('price', function ($medicine) {
                    return "Rp " . number_format($medicine->price, 2) ?? "";
                })
                ->editColumn('stock', function ($medicine) {
                    return $medicine->stock ?? "";
                })
                ->addColumn('action', function ($medicine) {
                    $token = csrf_token();
                    $btn =
                        '<form action="' . url('dashboard/sales/addmedicine', $medicine->id) . '" method="POST">
                            <input type="hidden" name="_token" value="' . $token . '">
                            <button type="submit" class="btn btn-primary btn-sm cart-btn"><i class="fas fa-cart-plus"> Tambah </i></button>
                            </form>';

                    if ($medicine->stock == 0) {
                        $btn = '<p class="btn btn-danger btn-sm cart-btn disable">
                                            <i class="far fa-window-close""> Stock Habis </i>
                                        </p>';
                    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        //cart item
        // if (request()->tax) {
        //     $tax = "+10%";
        // } else {
        //     $tax = "0%";
        // }

        $condition = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'pajak',
            'type' => 'tax', 
            'target' => 'total', 
            'value' => $tax = "+10%", 
            'order' => 1
        ));

        \Cart::session(Auth()->id())->condition($condition);

        $items = \Cart::session(Auth()->id())->getContent();
        

        if (\Cart::isEmpty()) {
            $cart_data = [];
        } else {
            foreach ($items as $row) {
                $cart[] = [
                    'rowId' => $row->id,
                    'name' => $row->name,
                    'stock' => $row->quantity,
                    'pricesingle' => $row->price,
                    'price' => $row->getPriceSum(),
                    'created_at' => $row->attributes['created_at'],
                ];
            }
            $cart_data = collect($cart)->sortBy('created_at');
        }
        
        //total
        $sub_total = \Cart::session(Auth()->id())->getSubTotal();
        $total = \Cart::session(Auth()->id())->getTotal();

        $new_condition = \Cart::session(Auth()->id())->getCondition('pajak');
        $pajak = $new_condition->getCalculatedValue($sub_total);

        $data_total = [
            'sub_total' => $sub_total,
            'total' => $total,
            'tax' => $pajak
        ];

        return view('dashboard.transaction.sales.index', compact('cart_data', 'data_total'));
    }



    public function addMedicineCart($id)
    {
        $medicines = Medicine::find($id);

        $cart = \Cart::session(Auth()->id())->getContent();

        $cek_itemId = $cart->whereIn('id', $id);

        if ($cek_itemId->isNotEmpty()) {
            if ($medicines->stock == $cek_itemId[$id]->quantity) {
                return redirect()->back()->with('error');
            } else {
                \Cart::session(Auth()->id())->update($id, array(
                    'quantity' => 1
                ));
            }
        } else {
            \Cart::session(Auth()->id())->add(array(
                'id' => $id,
                'name' => $medicines->name,
                'price' => $medicines->price,
                'quantity' => 1,
                'attributes' => array(
                    'created_at' => date('Y-m-d H:i:s')
                )
            ));

        }

        return redirect()->back();
    }


    public function removeMedicineCart($id)
    {
        \Cart::session(Auth()->id())->remove($id);

        return redirect()->back();
    }


    public function bayar()
    {


        $cart_total = \Cart::session(Auth()->id())->getTotal();
        $bayar = request()->bayar;
        $kembalian = (int)$bayar - (int)$cart_total;

        if ($kembalian >= 0) {
            DB::beginTransaction();

            try {

                $all_cart = \Cart::session(Auth()->id())->getContent();


                $filterCart = $all_cart->map(function ($item) {
                    return [
                        'id' => $item->id,
                        'quantity' => $item->quantity
                    ];
                });

                foreach ($filterCart as $cart) {
                    $medicine = Medicine::find($cart['id']);

                    if ($medicine->stock == 0) {
                        return redirect()->back()->with('errorTransaksi');
                    }

                    HistoryItem::create([
                        'medicine_id' => $cart['id'],
                        'user_id' => Auth::id(),
                        'stock' => $medicine->stock,
                        'stockChange' => -$cart['quantity'],
                        'last_stock' => $medicine->stock - $cart['quantity'],
                        'tipe' => 'Berkurang Karena Kegiatan Transaksi'
                    ]);

                    $medicine->decrement('stock', $cart['quantity']);
                }

                $id = IdGenerator::generate(['table' => 'invoices', 'field' => 'invoices_number', 'length' => 10, 'prefix' => 'INV-',]);

                $sub_total = \Cart::session(Auth()->id())->getSubTotal();
                $new_condition = \Cart::session(Auth()->id())->getCondition('pajak');
                $pajak = $new_condition->getCalculatedValue($sub_total);

                Invoice::create([
                    'invoices_number' => $id,
                    'user_id' => Auth::id(),
                    'pay' => request()->bayar,
                    'tax' => $pajak,
                    'total' => $cart_total,
                ]);

                foreach ($filterCart as $cart) {

                    PurchaseItem::create([
                        'medicine_id' => $cart['id'],
                        'invoices_number' => Invoice::where('invoices_number', '=', $id)->first()->id,
                        'stock' => $cart['quantity'],
                    ]);
                }

                // try {
                //     // /* Printer connection */
                //         // $connector = new WindowsPrintConnector("XP-58"); // Add connector for your printer here.
                //         // $printer = new Printer($connector);

                //         // /*data will print*/
                //         $buy_cart = \Cart::session(Auth()->id())->getContent();
                //         $address = "Jl. Pabuaran, Pabuaran, Cibinong, Bogor, Jawa Barat 16916";
                //         $date = "Tanggal Pembelian : " . Carbon::now()->toDateString();
                //         $new_condition = \Cart::session(Auth()->id())->getCondition('pajak');
                //         $sub_total = \Cart::session(Auth()->id())->getSubTotal();
                //         $tax = $new_condition->getCalculatedValue($sub_total);
                //         $total = \Cart::session(Auth()->id())->getTotal();


                //         // /* Line spacing */
                //         // /*
                //         // $printer -> setEmphasis(true);
                //         // $printer -> text("Line spacing\n");
                //         // $printer -> setEmphasis(false);
                //         // foreach(array(16, 32, 64, 128, 255) as $spacing) {
                //         //     $printer -> setLineSpacing($spacing);
                //         //     $printer -> text("Spacing $spacing: The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.\n");
                //         // }
                //         // $printer -> setLineSpacing(); // Back to default
                //         // */

                //         // /* Stuff around with left margin */
                //         // $printer->setEmphasis(true);
                //         // $printer->text("Left margin\n");
                //         // $printer->setEmphasis(false);
                //         // $printer->text("Default left\n");
                //         // foreach (array(1, 2, 4, 8, 16, 32, 64, 128, 256, 512) as $margin) {
                //         //     $printer->setPrintLeftMargin($margin);
                //         //     $printer->text("left margin $margin\n");
                //         // }
                //         // /* Reset left */
                //         // $printer->setPrintLeftMargin(0);

                //         // /* Stuff around with page width */
                //         // $printer->setEmphasis(true);
                //         // $printer->text("Page width\n");
                //         // $printer->setEmphasis(false);
                //         // $printer->setJustification(Printer::JUSTIFY_RIGHT);
                //         // $printer->text("Default width\n");
                //         // foreach (array(512, 256, 128, 64) as $width) {
                //         //     $printer->setPrintWidth($width);
                //         //     $printer->text("page width $width\n");
                //         // }

                //         // /* Printer shutdown */
                //         // $printer->cut();
                //         // $printer->close();

                //         // ------------------------------------------------------------------------------------------- container 
                //         // $connector = new WindowsPrintConnector("XP-58");
                //         // $printer = new Printer($connector);
                //         // // $logo = EscposImage::load("public/images/escpos.png");
                //         // $buy_cart = \Cart::session(Auth()->id())->getContent();
                //         // $address = "Jl. Pabuaran, Pabuaran, Cibinong, Bogor, Jawa Barat 16916";
                //         // $date = "Tanggal Pembelian : " . Carbon::now()->toDateString();
                //         // $new_condition = \Cart::session(Auth()->id())->getCondition('pajak');
                //         // $sub_total = \Cart::session(Auth()->id())->getSubTotal();
                //         // $tax = $new_condition->getCalculatedValue($sub_total);
                //         // $total = \Cart::session(Auth()->id())->getTotal();
                //         // /* Print top logo */
                //         //             // $printer->setJustification(Printer::JUSTIFY_CENTER);
                //         //             // // $printer->graphics($logo);
                //         //             // /* Name of shop */
                //         //             // $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
                //         //             // $printer->text("APOTIK SALAM.\n");
                //         //             // $printer->selectPrintMode();
                //         //             // $printer->text($address);
                //         //             // $printer->text($date . "\n");
                //         //             // $printer->text("Nomor Invoice : " . $id);
                //         //             // $printer->feed(2);
                //         //             // /* Title of receipt */
                //         //             // $printer->setEmphasis(true);
                //         //             // $printer->text("SALES INVOICE");
                //         //             // $printer->feed();
                //         //             // $printer->setEmphasis(false);
                //         //             // /* Items */

                //         // $printer->setEmphasis(true);
                //         // $iniNamaObat = [];
                //         // for ($i = 1; $i <= count($buy_cart); $i++) {

                //         //     $iniNamaObat[] .= $i . ". " . $buy_cart[$i]['name'];
                //         //     // $printer->text($i . ". " . $buy_cart[$i]['name'] . " " . $buy_cart[$i]['quantity'] . " : Rp. " . $buy_cart[$i]['price'] . "\n");
                //         // }

                //         // $iniHargaObat = [];
                //         // for ($i = 1; $i <= count($buy_cart); $i++) {

                //         //     $iniHargaObat[] .= " Rp. " . $buy_cart[$i]['price'];
                //         //     // $printer->text($i . ". " . $buy_cart[$i]['name'] . " " . $buy_cart[$i]['quantity'] . " : Rp. " . $buy_cart[$i]['price'] . "\n");
                //         // }
                //         // $strNamaObat = implode(' ',$iniNamaObat);
                //         // $strHargaObat = implode(' ',$iniHargaObat);

                //         // // dd($strNamaObat);
                //         // function columnify($strNamaObat, $strHargaObat, $leftWidth = 12, $rightWidth = 24, $space = 4)
                //         // {
                //         //     $leftWrapped = wordwrap($strNamaObat, $leftWidth, "\n", true);
                //         //     $rightWrapped = wordwrap($strHargaObat, $rightWidth, "\n", true);

                //         //     $leftLines = explode("\n", $leftWrapped);
                //         //     $rightLines = explode("\n", $rightWrapped);
                //         //     $allLines = [];
                //         //     for ($i = 0; $i < max(count($leftLines), count($rightLines)); $i++) {
                //         //         $leftPart = str_pad(isset($leftLines[$i]) ? $leftLines[$i] : "", $leftWidth, " ");
                //         //         $rightPart = str_pad(isset($rightLines[$i]) ? $rightLines[$i] : "", $rightWidth, " ");
                //         //         $allLines[] = $leftPart . str_repeat(" ", $space) . $rightPart;
                //         //     }
                //         //     return implode($allLines, "\n") . "\n";
                //         // }
                //         // // dd(columnify($strNamaObat, $strHargaObat));
                //         // $printer->text(columnify($strNamaObat, $strHargaObat));
                //         // $printer->feed();
                //         // /*subtotal*/
                //         // $printer->setJustification(Printer::JUSTIFY_LEFT);
                //         // $printer->setEmphasis(true);
                //         // $printer->text('Subtotal :');
                //         // $printer->text(' Rp. ' . $sub_total . "\n");
                //         // /* Tax and total */
                //         // $printer->text('Total :');
                //         // $printer->text(' Rp. ' . $tax . "\n");
                //         // $printer->text('Total :');
                //         // $printer->text(' Rp. ' . $total . "\n");
                //         // /* Footer */
                //         // $printer->feed(2);
                //         //             // $printer->setJustification(Printer::JUSTIFY_CENTER);
                //         //             // $printer->text("Terimakasih telah berbelanja di apotik Salam\n");
                //         //             // $printer->text("For trading hours, please visit example.com\n");
                //         //             // $printer->feed(2);
                //         // /* Cut the receipt and open the cash drawer */
                //         // $printer->cut();
                //         // $printer->pulse();
                //         // $printer->close();
                //         // /* A wrapper to do organise item names & prices into columns */
                //         //---------------------------------------------------------------------------------------------------- /container
                //         function addSpaces($string = '', $valid_string_length = 0) {
                //             if (strlen($string) < $valid_string_length) {
                //                 $spaces = $valid_string_length - strlen($string);
                //                 for ($index1 = 1; $index1 <= $spaces; $index1++) {
                //                     $string = $string . ' ';
                //                 }
                //             }

                //             return $string;
                //         }

                //         $connector = new WindowsPrintConnector("XP-58");
                //         $printer = new Printer($connector);


                //         $printer->feed();
                //         $printer->setPrintLeftMargin(0);
                //         $printer->setJustification(Printer::JUSTIFY_LEFT);
                //         $printer->setEmphasis(true);
                //         $printer->text(addSpaces('Item', 10) . addSpaces('QtyxPrice', 10) . addSpaces('Tot(f)', 8) . "\n");
                //         $printer->setEmphasis(false);


                //         for($i = 1; $i <= count($buy_cart); $i++ ) {

                //             if(isset($buy_cart[$i])){
                //                 //Current item ROW 1
                //             $name_lines = str_split($buy_cart[$i]['name'], 15);
                //             foreach ($name_lines as $k => $l) {
                //                 $l = trim($l);
                //                 $name_lines[$k] = addSpaces($l, 20);
                //             }

                //             $qtyx_price = str_split($buy_cart[$i]['quantity'], 15);
                //             foreach ($qtyx_price as $k => $l) {
                //                 $l = trim($l);
                //                 $qtyx_price[$k] = addSpaces($l, 20);
                //             }

                //             $total_price = str_split($buy_cart[$i]['price'], 8);
                //             foreach ($total_price as $k => $l) {
                //                 $l = trim($l);
                //                 $total_price[$k] = addSpaces($l, 8);
                //             }

                //             $counter = 0;
                //             $temp = [];
                //             $temp[] = count($name_lines);
                //             $temp[] = count($qtyx_price);
                //             $temp[] = count($total_price);
                //             $counter = max($temp);

                //             for ($ii = 0; $ii < $counter; $ii++) {
                //                 $line = '';
                //                 if (isset($name_lines[$ii])) {
                //                     $line .= ($name_lines[$ii]);
                //                 }
                //                 if (isset($qtyx_price[$ii])) {
                //                     $line .= ($qtyx_price[$ii]);
                //                 }
                //                 if (isset($total_price[$ii])) {
                //                     $line .= ($total_price[$ii]);
                //                 }
                //                 $printer->text($line . "\n");
                //             }

                //             }

                //             $printer->feed();
                //         }
                //         $printer->feed();
                //         /*subtotal*/
                //         $printer->setJustification(Printer::JUSTIFY_LEFT);
                //         $printer->setEmphasis(true);
                //         $printer->text('Subtotal :');
                //         $printer->text(' Rp. ' . $sub_total . "\n");
                //         /* Tax and total */
                //         $printer->text('Total :');
                //         $printer->text(' Rp. ' . $tax . "\n");
                //         $printer->text('Total :');
                //         $printer->text(' Rp. ' . $total . "\n");
                //         /* Footer */
                //         $printer->feed(2);
                //         $printer->setJustification(Printer::JUSTIFY_CENTER);
                //         $printer->text("Terimakasih telah berbelanja di apotik Salam\n");
                //         $printer->text("For trading hours, please visit example.com\n");
                //         $printer->feed(2);
                //         /* Cut the receipt and open the cash drawer */
                //         $printer->cut();
                //         $printer->pulse();
                //         $printer->close();

                // } catch(Exception $e) {
                //     echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
                // }

                // try {
                //     // /* Printer connection */
                //         // $connector = new WindowsPrintConnector("XP-58"); // Add connector for your printer here.
                //         // $printer = new Printer($connector);

                //         // /*data will print*/
                //         $buy_cart = \Cart::session(Auth()->id())->getContent();
                //         $address = "Jl. Pabuaran, Pabuaran, Cibinong, Bogor, Jawa Barat 16916";
                //         $date = "Tanggal Pembelian : " . Carbon::now()->toDateString();
                //         $new_condition = \Cart::session(Auth()->id())->getCondition('pajak');
                //         $sub_total = \Cart::session(Auth()->id())->getSubTotal();
                //         $tax = $new_condition->getCalculatedValue($sub_total);
                //         $total = \Cart::session(Auth()->id())->getTotal();


                //         // /* Line spacing */
                //         // /*
                //         // $printer -> setEmphasis(true);
                //         // $printer -> text("Line spacing\n");
                //         // $printer -> setEmphasis(false);
                //         // foreach(array(16, 32, 64, 128, 255) as $spacing) {
                //         //     $printer -> setLineSpacing($spacing);
                //         //     $printer -> text("Spacing $spacing: The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.\n");
                //         // }
                //         // $printer -> setLineSpacing(); // Back to default
                //         // */

                //         // /* Stuff around with left margin */
                //         // $printer->setEmphasis(true);
                //         // $printer->text("Left margin\n");
                //         // $printer->setEmphasis(false);
                //         // $printer->text("Default left\n");
                //         // foreach (array(1, 2, 4, 8, 16, 32, 64, 128, 256, 512) as $margin) {
                //         //     $printer->setPrintLeftMargin($margin);
                //         //     $printer->text("left margin $margin\n");
                //         // }
                //         // /* Reset left */
                //         // $printer->setPrintLeftMargin(0);

                //         // /* Stuff around with page width */
                //         // $printer->setEmphasis(true);
                //         // $printer->text("Page width\n");
                //         // $printer->setEmphasis(false);
                //         // $printer->setJustification(Printer::JUSTIFY_RIGHT);
                //         // $printer->text("Default width\n");
                //         // foreach (array(512, 256, 128, 64) as $width) {
                //         //     $printer->setPrintWidth($width);
                //         //     $printer->text("page width $width\n");
                //         // }

                //         // /* Printer shutdown */
                //         // $printer->cut();
                //         // $printer->close();

                //         // ------------------------------------------------------------------------------------------- container 
                //         // $connector = new WindowsPrintConnector("XP-58");
                //         // $printer = new Printer($connector);
                //         // // $logo = EscposImage::load("public/images/escpos.png");
                //         // $buy_cart = \Cart::session(Auth()->id())->getContent();
                //         // $address = "Jl. Pabuaran, Pabuaran, Cibinong, Bogor, Jawa Barat 16916";
                //         // $date = "Tanggal Pembelian : " . Carbon::now()->toDateString();
                //         // $new_condition = \Cart::session(Auth()->id())->getCondition('pajak');
                //         // $sub_total = \Cart::session(Auth()->id())->getSubTotal();
                //         // $tax = $new_condition->getCalculatedValue($sub_total);
                //         // $total = \Cart::session(Auth()->id())->getTotal();
                //         // /* Print top logo */
                //         //             // $printer->setJustification(Printer::JUSTIFY_CENTER);
                //         //             // // $printer->graphics($logo);
                //         //             // /* Name of shop */
                //         //             // $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
                //         //             // $printer->text("APOTIK SALAM.\n");
                //         //             // $printer->selectPrintMode();
                //         //             // $printer->text($address);
                //         //             // $printer->text($date . "\n");
                //         //             // $printer->text("Nomor Invoice : " . $id);
                //         //             // $printer->feed(2);
                //         //             // /* Title of receipt */
                //         //             // $printer->setEmphasis(true);
                //         //             // $printer->text("SALES INVOICE");
                //         //             // $printer->feed();
                //         //             // $printer->setEmphasis(false);
                //         //             // /* Items */

                //         // $printer->setEmphasis(true);
                //         // $iniNamaObat = [];
                //         // for ($i = 1; $i <= count($buy_cart); $i++) {

                //         //     $iniNamaObat[] .= $i . ". " . $buy_cart[$i]['name'];
                //         //     // $printer->text($i . ". " . $buy_cart[$i]['name'] . " " . $buy_cart[$i]['quantity'] . " : Rp. " . $buy_cart[$i]['price'] . "\n");
                //         // }

                //         // $iniHargaObat = [];
                //         // for ($i = 1; $i <= count($buy_cart); $i++) {

                //         //     $iniHargaObat[] .= " Rp. " . $buy_cart[$i]['price'];
                //         //     // $printer->text($i . ". " . $buy_cart[$i]['name'] . " " . $buy_cart[$i]['quantity'] . " : Rp. " . $buy_cart[$i]['price'] . "\n");
                //         // }
                //         // $strNamaObat = implode(' ',$iniNamaObat);
                //         // $strHargaObat = implode(' ',$iniHargaObat);

                //         // // dd($strNamaObat);
                //         // function columnify($strNamaObat, $strHargaObat, $leftWidth = 12, $rightWidth = 24, $space = 4)
                //         // {
                //         //     $leftWrapped = wordwrap($strNamaObat, $leftWidth, "\n", true);
                //         //     $rightWrapped = wordwrap($strHargaObat, $rightWidth, "\n", true);

                //         //     $leftLines = explode("\n", $leftWrapped);
                //         //     $rightLines = explode("\n", $rightWrapped);
                //         //     $allLines = [];
                //         //     for ($i = 0; $i < max(count($leftLines), count($rightLines)); $i++) {
                //         //         $leftPart = str_pad(isset($leftLines[$i]) ? $leftLines[$i] : "", $leftWidth, " ");
                //         //         $rightPart = str_pad(isset($rightLines[$i]) ? $rightLines[$i] : "", $rightWidth, " ");
                //         //         $allLines[] = $leftPart . str_repeat(" ", $space) . $rightPart;
                //         //     }
                //         //     return implode($allLines, "\n") . "\n";
                //         // }
                //         // // dd(columnify($strNamaObat, $strHargaObat));
                //         // $printer->text(columnify($strNamaObat, $strHargaObat));
                //         // $printer->feed();
                //         // /*subtotal*/
                //         // $printer->setJustification(Printer::JUSTIFY_LEFT);
                //         // $printer->setEmphasis(true);
                //         // $printer->text('Subtotal :');
                //         // $printer->text(' Rp. ' . $sub_total . "\n");
                //         // /* Tax and total */
                //         // $printer->text('Total :');
                //         // $printer->text(' Rp. ' . $tax . "\n");
                //         // $printer->text('Total :');
                //         // $printer->text(' Rp. ' . $total . "\n");
                //         // /* Footer */
                //         // $printer->feed(2);
                //         //             // $printer->setJustification(Printer::JUSTIFY_CENTER);
                //         //             // $printer->text("Terimakasih telah berbelanja di apotik Salam\n");
                //         //             // $printer->text("For trading hours, please visit example.com\n");
                //         //             // $printer->feed(2);
                //         // /* Cut the receipt and open the cash drawer */
                //         // $printer->cut();
                //         // $printer->pulse();
                //         // $printer->close();
                //         // /* A wrapper to do organise item names & prices into columns */
                //         //---------------------------------------------------------------------------------------------------- /container
                //         function addSpaces($string = '', $valid_string_length = 0) {
                //             if (strlen($string) < $valid_string_length) {
                //                 $spaces = $valid_string_length - strlen($string);
                //                 for ($index1 = 1; $index1 <= $spaces; $index1++) {
                //                     $string = $string . ' ';
                //                 }
                //             }

                //             return $string;
                //         }

                //         $connector = new WindowsPrintConnector("XP-58");
                //         $printer = new Printer($connector);


                //         $printer->feed();
                //         $printer->setPrintLeftMargin(0);
                //         $printer->setJustification(Printer::JUSTIFY_LEFT);
                //         $printer->setEmphasis(true);
                //         $printer->text(addSpaces('Item', 10) . addSpaces('QtyxPrice', 10) . addSpaces('Tot(f)', 8) . "\n");
                //         $printer->setEmphasis(false);


                //         for($i = 1; $i <= count($buy_cart); $i++ ) {

                //             if(isset($buy_cart[$i])){
                //                 //Current item ROW 1
                //             $name_lines = str_split($buy_cart[$i]['name'], 15);
                //             foreach ($name_lines as $k => $l) {
                //                 $l = trim($l);
                //                 $name_lines[$k] = addSpaces($l, 20);
                //             }

                //             $qtyx_price = str_split($buy_cart[$i]['quantity'], 15);
                //             foreach ($qtyx_price as $k => $l) {
                //                 $l = trim($l);
                //                 $qtyx_price[$k] = addSpaces($l, 20);
                //             }

                //             $total_price = str_split($buy_cart[$i]['price'], 8);
                //             foreach ($total_price as $k => $l) {
                //                 $l = trim($l);
                //                 $total_price[$k] = addSpaces($l, 8);
                //             }

                //             $counter = 0;
                //             $temp = [];
                //             $temp[] = count($name_lines);
                //             $temp[] = count($qtyx_price);
                //             $temp[] = count($total_price);
                //             $counter = max($temp);

                //             for ($ii = 0; $ii < $counter; $ii++) {
                //                 $line = '';
                //                 if (isset($name_lines[$ii])) {
                //                     $line .= ($name_lines[$ii]);
                //                 }
                //                 if (isset($qtyx_price[$ii])) {
                //                     $line .= ($qtyx_price[$ii]);
                //                 }
                //                 if (isset($total_price[$ii])) {
                //                     $line .= ($total_price[$ii]);
                //                 }
                //                 $printer->text($line . "\n");
                //             }

                //             }

                //             $printer->feed();
                //         }
                //         $printer->feed();
                //         /*subtotal*/
                //         $printer->setJustification(Printer::JUSTIFY_LEFT);
                //         $printer->setEmphasis(true);
                //         $printer->text('Subtotal :');
                //         $printer->text(' Rp. ' . $sub_total . "\n");
                //         /* Tax and total */
                //         $printer->text('Total :');
                //         $printer->text(' Rp. ' . $tax . "\n");
                //         $printer->text('Total :');
                //         $printer->text(' Rp. ' . $total . "\n");
                //         /* Footer */
                //         $printer->feed(2);
                //         $printer->setJustification(Printer::JUSTIFY_CENTER);
                //         $printer->text("Terimakasih telah berbelanja di apotik Salam\n");
                //         $printer->text("For trading hours, please visit example.com\n");
                //         $printer->feed(2);
                //         /* Cut the receipt and open the cash drawer */
                //         $printer->cut();
                //         $printer->pulse();
                //         $printer->close();

                // } catch(Exception $e) {
                //     echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
                // }


                \Cart::session(Auth()->id())->clear();
                DB::commit();
                return redirect()->back()->with('success', 'Transaksi Berhasil dilakukan Tahu Coding | Klik History untuk print');
            } catch (\Exeception $e) {
                DB::rollback();
                return redirect()->back()->with('errorTransaksi', 'jumlah pembayaran gak valid');
            }
        }
        return redirect()->back()->with('errorTransaksi', 'jumlah pembayaran gak valid');
    }


    public function clear()
    {
        \Cart::session(Auth()->id())->clear();
        return redirect()->back();
    }

    public function decreasecart($id)
    {

        $cart = \Cart::session(Auth()->id())->getContent();
        $cek_itemId = $cart->whereIn('id', $id);

        if ($cek_itemId[$id]->quantity == 1) {
            \Cart::session(Auth()->id())->remove($id);
        } else {
            \Cart::session(Auth()->id())->update($id, array(
                'quantity' => array(
                    'relative' => true,
                    'value' => -1
                )
            ));
        }
        return redirect()->back();
    }


    public function increasecart($id)
    {
        $medicine = Medicine::find($id);

        $cart = \Cart::session(Auth()->id())->getContent();
        $cek_itemId = $cart->whereIn('id', $id);

        if ($medicine->stock == $cek_itemId[$id]->quantity) {
            return redirect()->back()->with('error', 'jumlah item kurang');
        } else {
            \Cart::session(Auth()->id())->update($id, array(
                'quantity' => array(
                    'relative' => true,
                    'value' => 1
                )
            ));

            return redirect()->back();
        }
    }

    public function history(Request $request)
    {
        //record data selama hari ini
        $total_today = Invoice::whereDate('created_at', Carbon::today())->get()->sum('total');

        //record data selama bulan ini
        $fromDate = Carbon::now()->startOfMonth()->toDateString();
        $tillDate = Carbon::now()->endOfMonth()->toDateString();
        $total_this_month = Invoice::whereBetween('created_at', [$fromDate, $tillDate])->get()->sum('total');

        //record data selama tahun ini
        $total_this_year = Invoice::whereYear('created_at', Carbon::now()->year)->get()->sum('total');

        if ($request->ajax()) {
            $history = Invoice::all();
            return DataTables::of($history)
                ->addIndexColumn()
                ->editColumn('total', function ($history) {
                    return "Rp " . number_format($history->total, 2) ?? "";
                })
                ->editColumn('pay', function ($history) {
                    return "Rp " . number_format($history->pay, 2) ?? "";
                })
                ->editColumn('tax', function ($history) {
                    return "Rp " . number_format($history->tax, 2) ?? "";
                })
                ->editColumn('user', function ($history) {
                    return $history->user->name ?? "";
                })
                ->editColumn('created_at', function ($history) {
                    return $history->created_at->toDateString() ?? '';
                })
                ->addColumn('action', function ($history) {
                    $btn =
                        '
                        <a href="' . url('dashboard/sales/invoice', $history->id) . '" class="btn btn-primary btn-sm"><i class="fas fa-eye"> Rincian</i></a>
                        ';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('dashboard.transaction.sales.history', compact('total_today', 'total_this_month', 'total_this_year'));
    }

    public function invoice($id)
    {
        $invoice = Invoice::with('purchaseItem')->find($id);
        return view('dashboard.transaction.report.invoice', compact('invoice'));
    }
}
