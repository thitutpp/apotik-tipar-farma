<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Database\Eloquent\Concerns\HasEvents;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Pengguna', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy']]);
    }
    
    // function __construct()
    // {
    //     $this->middleware('permission:Akses Daftar Pengguna', ['only' => ['index', 'show']]);
    //     $this->middleware('permission:Akses Tambah Pengguna', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:Akses Edit Pengguna', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:Akses Hapus Pengguna Secara Permanen', ['only' => ['destroy']]);
    // }

    public function index(Request $request) 
    {

        $user = User::all();
        return view('dashboard.userManagement.users.index', compact('user'))->with('no');
    }
   
    public function create()
    {
        $roles = Role::all()->pluck('name', 'id')->prepend(trans('Pilih Peranan'), '');
        return view('dashboard.userManagement.users.create', compact('roles'));
    }
  
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        $message = 'Data user berhasil di ditambahkan';
        return redirect()->route('users.index')->with('success', $message);
    }
       
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all()->pluck('name', 'id')->prepend(trans('Pilih Peranan'), '');
        $userRole = DB::table("model_has_roles")->where("model_has_roles.model_id", $id)->pluck('model_has_roles.role_id', 'model_has_roles.role_id')->all();
        return view('dashboard.userManagement.users.edit', compact('user', 'roles', 'userRole'));
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required|email|unique:users,email,' . $id, 'password' => 'same:confirm-password', 'roles' => 'required']);
        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));
        $message = 'Data user berhasil di perbaruhi';
        return redirect()->route('users.index')->with('success', $message);
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        $message = 'Data user berhasil di hapus';
        return redirect()->route('users.index')->with('success', $message);
    }
}
