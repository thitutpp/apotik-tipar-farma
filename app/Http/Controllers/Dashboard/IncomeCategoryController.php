<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\IncomeCategory;
use Illuminate\Http\Request;

class IncomeCategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Biaya', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:categories-income-list', ['only' => ['index']]);
    //     $this->middleware('permission:categories-income-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:categories-income-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:categories-income-delete', ['only' => ['destroy']]);
    //     $this->middleware('permission:categories-income-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:categories-income-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:categories-income-permanentDelete', ['only' => ['permanentDelete']]);
    // }

    public function index()
    {
        $incomeCategories  = IncomeCategory::all();
        return view('dashboard.financialManagement.incomeCategories.index', compact('incomeCategories'))->with('no');
    }

    public function create()
    {
        return view('dashboard.financialManagement.incomeCategories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:income_categories,name',
        ]);

        IncomeCategory::create($request->all());

        $message = 'Data kategori pendapatan berhasil ditambahkan';
        return redirect()->route('income-categories.index')->with('success', $message);
    }

    public function edit($id)
    {
        $incomeCategories  = IncomeCategory::find($id);
        return view('dashboard.financialManagement.incomeCategories.edit', compact('incomeCategories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',

        ]);

        $incomeCategories  = IncomeCategory::find($id);
        $incomeCategories->name = $request->input('name');
        $incomeCategories->save();
        $message = 'Data kategori pendapatan berhasil diperbaruhi';
        return redirect()->route('income-categories.index')->with('success', $message);
    }

    public function destroy($id)
    {
        $incomeCategories  = IncomeCategory::find($id);
        $incomeCategories->delete();
        $message = "Data kategori pendapatan berhasil dihapus";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $incomeCategories  = IncomeCategory::onlyTrashed()->get();
        return view('dashboard.financialManagement.incomeCategories.trash', compact('incomeCategories'))->with('no');
    }

    public function restore($id)
    {
        $incomeCategories  = IncomeCategory::onlyTrashed()->where('id', $id);
        $incomeCategories->restore();
        $message = "Data kategori pendapatan berhasil dipulihkan";
        return redirect()->route('income-categories.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $incomeCategories  = IncomeCategory::onlyTrashed()->where('id', $id);
        $incomeCategories->forceDelete();
        $message = "Data kategori pendapatan berhasil dihapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
