<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Pengguna', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:role-list', ['only' => ['index', 'show']]);
    //     $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    // }

    public function index()
    {
        $roles = Role::all();
        return view('dashboard.userManagement.roles.index', compact('roles'))->with('no');
    }

    public function create()
    {
        $permission = Permission::all();
        return view('dashboard.userManagement.roles.create', compact('permission'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
        $message = 'Data role berhasil ditambahkan';
        return redirect()->route('roles.index')->with('success', $message);
    }

    public function show($id)
    {
        $role = Role::find($id);
        return view('dashboard.userManagement.roles.show', compact('role'));
    }

    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')->all();
        return view('dashboard.userManagement.roles.edit', compact('role', 'permission', 'rolePermissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permission'));
        $message = 'Data role berhasil diperbaruhi';
        return redirect()->route('roles.index')->with('success', $message);
    }

    public function destroy($id)
    {
        DB::table("roles")->where('id', $id)->delete();
        $message = 'Data role berhasil dihapus';
        return redirect()->route('roles.index')->with('success', $message);
    }
}
