<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\Expense;
use App\Model\Income;
use App\Model\Medicine;
use App\Model\MedicineCategory;
use App\Model\MedicineUnit;
use App\Model\PurchaseItem;
use App\Model\MedicineRetur;
use App\Model\Invoice;
use App\Model\User;
use Illuminate\Http\Request;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Dashboard', ['only' => ['index']]);
    }

    public function index()
    {
        $medicines = Medicine::count();
        $categories = MedicineCategory::count();
        $units = MedicineUnit::count();
        $return = MedicineRetur::count();
        $purchaseitem = PurchaseItem::count();


        $chart1_options = [
            'chart_title' => 'Grafik Pendapatan Keseluruhan',
            'report_type' => 'group_by_date',
            'model' => 'App\Model\Income',
            'group_by_field' => 'entry_date',
            'group_by_period' => 'month',
            'chart_type' => 'line',
            'aggregate_function'    => 'sum',
            'aggregate_field'       => 'amount',
            'filter_field'  => 'entry_date',
            'filter_days' => 365,

        ];
        $chart1 = new LaravelChart($chart1_options);

        $fromDate1 = Carbon::now()->startOfMonth()->toDateString();
        $tillDate1 = Carbon::now()->endOfMonth()->toDateString();
        $total_this_month1 = Income::whereBetween('entry_date',[$fromDate1,$tillDate1])->get()->sum('amount');



        $chart2_options = [
            'chart_title' => 'Grafik Pengeluaran Keseluruhan',
            'report_type' => 'group_by_date',
            'model' => 'App\Model\Expense',
            'group_by_field' => 'entry_date',
            'group_by_period' => 'month',
            'chart_type' => 'line',
            'aggregate_function'    => 'sum',
            'aggregate_field'       => 'amount',
            'filter_field'  => 'entry_date',
            'filter_days' => 365,
            

        ];
        $chart2 = new LaravelChart($chart2_options);

        $fromDate2 = Carbon::now()->startOfMonth()->toDateString();
        $tillDate2 = Carbon::now()->endOfMonth()->toDateString();
        $total_this_month2 = Expense::whereBetween('entry_date',[$fromDate2,$tillDate2])->get()->sum('amount');


        /* Laporan Transaksi */
        //record data selama hari ini
        $total_today = Invoice::whereDate('created_at', Carbon::today())->get()->sum('total');
        
        //record data selama bulan ini
        $fromDate = Carbon::now()->startOfMonth()->toDateString();
        $tillDate = Carbon::now()->endOfMonth()->toDateString();
        $total_this_month = Invoice::whereBetween('created_at',[$fromDate,$tillDate])->get()->sum('total');
        
        //record data selama tahun ini
        $total_this_year = Invoice::whereYear('created_at', Carbon::now()->year)->get()->sum('total');
        
        /* Laporan Transaksi */

        return view('dashboard.home', compact('medicines', 'categories', 'units', 'return', 'purchaseitem', 'chart1', 'chart2', 'total_this_month1', 'total_this_month2','total_today', 'total_this_month', 'total_this_year'));
    }
}
