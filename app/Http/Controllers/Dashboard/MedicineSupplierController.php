<?php

namespace App\Http\Controllers\Dashboard;

use App\Model\MedicineSupplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MedicineSupplierController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Produk', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:medicine-supplier-list', ['only' => ['index']]);
    //     $this->middleware('permission:medicine-supplier-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:medicine-supplier-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:medicine-supplier-delete', ['only' => ['destroy']]); 
    //     $this->middleware('permission:medicine-supplier-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:medicine-supplier-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:medicine-supplier-permanentDelete', ['only' => ['permanentDelete']]);
    // }

    public function index()
    {
        $medicineSuppliers = MedicineSupplier::all();
        return view('dashboard.productManagement.medicineSupplier.index', compact('medicineSuppliers'))->with('no');
    }

    public function create()
    {
        return view('dashboard.productManagement.medicineSupplier.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:medicine_suppliers,name',
        ]);

        MedicineSupplier::create($request->all());

        $message = 'Data perusahaan pemasok obat berhasil ditambahkan';
        return redirect()->route('medicine-suppliers.index')->with('success', $message);
    }

    public function edit($id)
    {
        $supplier = MedicineSupplier::find($id);
        return view('dashboard.productManagement.medicineSupplier.edit', compact('supplier'));
    }

    public function update(Request $request,  $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:medicine_suppliers,name',

        ]);

        $suppliers = MedicineSupplier::find($id);
        $suppliers->name = $request->input('name');
        $suppliers->save();
        $message = "Data perusahaan pemasok obat berhasil diperbaruhi";
        return redirect()->route('medicine-suppliers.index')->with('success', $message);
    }

    public function destroy($id)
    {
        $suppliers = MedicineSupplier::find($id);
        $suppliers->delete();
        $message = "Data perusahaan pemasok obat berhasil dipindah ke tong sampah";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $suppliers = MedicineSupplier::onlyTrashed()->get();
        return view('dashboard.productManagement.medicineSupplier.trash', compact('suppliers'))->with('no');
    }

    public function restore($id)
    {
        $suppliers = MedicineSupplier::onlyTrashed()->where('id', $id);
        $suppliers->restore();
        $message = "Data satuan berhasil di pulihkan";
        return redirect()->route('medicine-suppliers.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $suppliers = MedicineSupplier::onlyTrashed()->where('id', $id);
        $suppliers->forceDelete();
        $message = "Data satuan berhasil di hapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
