<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MedicineCategory;

class MedicineCategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Produk', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:medicine-categories-list', ['only' => ['index']]);
    //     $this->middleware('permission:medicine-categories-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:medicine-categories-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:medicine-categories-delete', ['only' => ['destroy']]);
    //     $this->middleware('permission:medicine-categories-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:medicine-categories-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:medicine-categories-permanentDelete', ['only' => ['permanentDelete']]);
    // }

    public function index()
    {
        $medicineCategories = MedicineCategory::all();
        return view('dashboard.productManagement.medicineCategories.index', compact('medicineCategories'))->with('no');
    }

    public function create()
    {
        return view('dashboard.productManagement.medicineCategories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:medicine_categories,name',
        ]);

        MedicineCategory::create($request->all());

        $message = 'Data kategori obat berhasil ditambahkan';
        return redirect()->route('medicine-categories.index')->with('success', $message);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $medicineCategories = MedicineCategory::find($id);
        return view('dashboard.productManagement.medicineCategories.edit', compact('medicineCategories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:medicine_categories,name',

        ]);

        $medicineCategories = MedicineCategory::find($id);
        $medicineCategories->name = $request->input('name');
        $medicineCategories->save();
        $message = 'Data kategori obat berhasil diperbaruhi';
        return redirect()->route('medicine-categories.index')->with('success', $message);
    }

    public function destroy($id)
    {
        $medicineCategories = MedicineCategory::find($id);
        $medicineCategories->delete();
        $message = "Data kategori obat berhasil dihapus";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $medicineCategories = MedicineCategory::onlyTrashed()->get();
        return view('dashboard.productManagement.medicineCategories.trash', compact('medicineCategories'))->with('no');
    }

    public function restore($id)
    {
        $medicineCategories = MedicineCategory::onlyTrashed()->where('id', $id);
        $medicineCategories->restore();
        $message = "Data kategori obat berhasil dipulihkan";
        return redirect()->route('medicine-categories.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $medicineCategories = MedicineCategory::onlyTrashed()->where('id', $id);
        $medicineCategories->forceDelete();
        $message = "Data kategori obat berhasil dihapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
