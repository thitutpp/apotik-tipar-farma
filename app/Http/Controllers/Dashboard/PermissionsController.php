<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class PermissionsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Pengguna', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy']]);
    }
    // function __construct()
    // {
    //     $this->middleware('permission:permissions-list', ['only' => ['index', 'show']]);
    //     $this->middleware('permission:permissions-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:permissions-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:permissions-delete', ['only' => ['destroy']]);
    // }

    public function index()
    {
        $permissions = Permission::all();
        return view('dashboard.userManagement.permissions.index', compact('permissions'))->with('no');
    }

    public function create()
    {
        return view('dashboard.userManagement.permissions.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
        ]);

        Permission::create($request->all());

        $message = 'Data permission berhasil ditambahkan';
        return redirect()->route('permissions.index')->with('success', $message);
    }

    public function edit($id)
    {

        $permissions = Permission::find($id);
        return view('dashboard.userManagement.permissions.edit', compact('permissions'));
    }

    public function update(Request $request, $id)
    {
        $permissions = Permission::find($id);
        $permissions->name = $request->input('name');
        $permissions->save();
        $message = "Data permissions berhasil diperbaruhi";
        return redirect()->route('permissions.index')->with('success', $message);
    }

    public function destroy($id)
    {
        DB::table("permissions")->where('id', $id)->delete();
        $message = "Data permissions berhasil dihapus";
        return redirect()->route('permissions.index')->with('success', $message);
    }
}
