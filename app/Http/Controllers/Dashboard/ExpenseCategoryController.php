<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\ExpenseCategory;
use Illuminate\Http\Request;


class ExpenseCategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Biaya', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:categories-expense-list', ['only' => ['index']]);
    //     $this->middleware('permission:categories-expense-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:categories-expense-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:categories-expense-delete', ['only' => ['destroy']]);
    //     $this->middleware('permission:categories-expense-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:categories-expense-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:categories-expense-permanentDelete', ['only' => ['permanentDelete']]);
    // }

    public function index()
    {

        $expenseCategories = ExpenseCategory::all();
        return view('dashboard.financialManagement.expenseCategories.index', compact('expenseCategories'))->with('no');
    }

    public function create()
    {
        return view('dashboard.financialManagement.expenseCategories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:expense_categories,name',
        ]);

        ExpenseCategory::create($request->all());
        $message = 'Data kategori pengeluaran berhasil ditambahkan';
        return redirect()->route('expense-categories.index')->with('success', $message);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $expenseCategories = ExpenseCategory::find($id);
        return view('dashboard.financialManagement.expenseCategories.edit', compact('expenseCategories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',

        ]);

        $expenseCategories = ExpenseCategory::find($id);
        $expenseCategories->name = $request->input('name');
        $expenseCategories->save();
        $message = 'Data kategori pengeluaran berhasil diperbaruhi';
        return redirect()->route('expense-categories.index')->with('success', $message);
    }

    public function destroy($id)
    {
        $expenseCategories = ExpenseCategory::find($id);
        $expenseCategories->delete();
        $message = "Data kategori pengeluaran berhasil dihapus";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $expenseCategories = ExpenseCategory::onlyTrashed()->get();
        return view('dashboard.financialManagement.expenseCategories.trash', compact('expenseCategories'))->with('no');
    }

    public function restore($id)
    {
        $expenseCategories = ExpenseCategory::onlyTrashed()->where('id', $id);
        $expenseCategories->restore();
        $message = "Data kategori pengeluaran berhasil dipulihkan";
        return redirect()->route('expense-categories.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $expenseCategories = ExpenseCategory::onlyTrashed()->where('id', $id);
        $expenseCategories->forceDelete();
        $message = "Data kategori pengeluaran berhasil dihapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
