<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Medicine;
use App\Model\MedicineCategory;
use App\Model\MedicineUnit;
use App\Model\HistoryItem;
use App\Model\MedicineReturCategory;
use App\Model\MedicineRetur;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use SebastianBergmann\Environment\Console;
use DataTables;
use Illuminate\Validation\ValidationException;

class MedicineReturController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Retur', ['only' => ['index', 'create', 'store', 'edit', 'update', 'allExpired']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:medicine-return-list', ['only' => ['index']]);
    //     $this->middleware('permission:medicine-return-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:medicine-return-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:medicine-expired-return-action', ['only' => ['allExpired']]);
    // }

    public function index(Request $request)
    {
        $medicines = Medicine::where('is_expired', TRUE)->get(); // akan menampilkan record dimana field is_expired == TRUE

        if ($request->ajax()) {
            $retur = MedicineRetur::all();
            // dd(DataTables::of($retur));
            return DataTables::of($retur)
                ->addIndexColumn()
                ->editColumn('code', function ($retur) {
                    return $retur->medicine->code ?? "";
                })
                ->editColumn('name', function ($retur) {
                    return $retur->medicine->name ?? "";
                })
                ->editColumn('unit', function ($retur) {
                    return $retur->medicine->unit->name ?? "";
                })
                ->editColumn('retur_category', function ($retur) {
                    return $retur->retur_category->name ?? "";
                })
                ->editColumn('supplier', function ($retur) {
                    return $retur->medicine->supplier->name ?? "";
                })
                ->editColumn('created_at', function ($retur) {
                    return Carbon::createFromFormat('Y-m-d H:i:s', $retur->created_at)->format('Y-m-d') ?? "";
                })
                ->addColumn('action', function ($retur) {
                    $btn = '';
                    // if (Auth::user()->roles->pluck('name')[0] == 'dashboard.) {
                    $btn =
                        '<a href="' . route('medicine-return.edit', $retur->id) . '" class="edit btn btn-primary btn-sm"><i class="far fa-edit"> Ubah</i></a>';
                    //    }
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('dashboard.productManagement.return.index', compact('medicines'))->with('no');
    }

    public function create()
    {
        $medicines = Medicine::orderBy('supplier_id')->get();
        $returCategory = MedicineReturCategory::all()->pluck('name', 'id')->prepend(trans('Pilih Jenis Retur'), '');
        return view('dashboard.productManagement.return.create', compact('medicines', 'returCategory'));
    }

    public function allExpired($id)
    {
        DB::beginTransaction();
        try {
            $medicine = Medicine::find($id);
            // $cek = ($medicine->stock) * -1;
            // dd($cek);
            if ($medicine) {
                MedicineRetur::create([
                    'medicine_id' => $id,
                    'retur_category_id' => 1,
                    'user_id' => Auth::id(),
                    'qty_return' => $medicine->stock,
                ]);
                $returCategory = MedicineReturCategory::find(1);
                HistoryItem::create([
                    'medicine_id' => $medicine->id,
                    'user_id' => Auth::id(),
                    'stock' => $medicine->stock,
                    'stockChange' => ($medicine->stock) * -1,
                    'last_stock' => $medicine->stock + ($medicine->stock) * -1,
                    'tipe' => $returCategory->name,
                ]);

                Medicine::where('id', $id)
                    ->update([
                        'expired' => NULL,
                        'is_expired' => FALSE,
                        'stock' => 0
                    ]);
            }

            $message = 'Data obat expired berhasil dicatat';

            DB::commit();
            return redirect()->route('medicine-return.index')->with('success', $message);
        } catch (\Exeception $e) {
            DB::rollback();
            return redirect()->route('medicine-return.index')->with('error', $e);
        }
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'qty_return' => 'numeric|min:1|required|',
            'medicine_id' => 'required',
            'retur_category_id' => 'required'
        ]);

        $medicine = Medicine::find($request->medicine_id);

        if ($request->qty_return > $medicine->stock) {
            throw ValidationException::withMessages(['qty_return' => 'Kuantitas yang dimasukan melebihi kuantitas data obat saat ini.']);
        }
        DB::beginTransaction();
        try {
            MedicineRetur::create([
                'medicine_id' => $request->medicine_id,
                'retur_category_id' => $request->retur_category_id,
                'user_id' => Auth::id(),
                'qty_return' => $request->qty_return,
            ]);

            $returCategory = MedicineReturCategory::find($request->retur_category_id);
            HistoryItem::create([
                'medicine_id' => $request->medicine_id,
                'user_id' => Auth::id(),
                'stock' => $medicine->stock,
                'stockChange' => ($request->qty_return) * -1,
                'last_stock' => ($medicine->stock + ($request->qty_return * -1)),
                'tipe' => $returCategory->name

            ]);

            Medicine::where('id', $request->medicine_id)
                ->update([
                    'stock' => $medicine->stock - $request->qty_return
                ]);

            $message = 'Data obat berhasil di ditambahkan';

            DB::commit();
            return redirect()->route('medicine-return.index')->with('success', $message);
        } catch (\Exeception $e) {
            DB::rollback();
            return redirect()->route('medicine-return.create')->with('error', $e);
        }
    }

    public function edit($id)
    {
        $retur = MedicineRetur::find($id);
        $medicines = Medicine::all()->prepend(trans('Pilih Obat Diretur'), '');
        $returCategory = MedicineReturCategory::all()->pluck('name', 'id')->prepend(trans('Pilih Jenis Retur'), '');
        // dd($returCategory);
        return view('dashboard.productManagement.return.edit', compact('retur', 'returCategory'));
    }

    public function update(Request $request, $id)
    {

        $message = 'Data retur berhasil diperbarui';


        DB::beginTransaction();
        try {

            $id = $request->id != NULL ? $request->id : $request->rollback;
            // dd($request->rollback);
            $retur = MedicineRetur::find($id);
            // dd($retur);
            $medicine = Medicine::find($retur->medicine_id);

            // mengembalikan error jika kuantitas yang dimasukan melebihi data retur
            //5 < 0
            if (($retur->qty_return + $request->qty_return) < 0) {
                throw ValidationException::withMessages(['qty_return' => 'Kuantitas yang dimasukan melebihi kuantitas retur saat ini.']);
            }

            // mengembalikan error jika kuantitas yang dimasukan melebihi kuantitas obat saat ini
            //19+10 < 10+-5
            //29 < 5
            if (($medicine->stock + $retur->qty_return) < ($retur->qty_return + $request->qty_return) || ($retur->qty_return + $request->qty_return) < 0) {
                throw ValidationException::withMessages(['qty_return' => 'Kuantitas yang dimasukan melebihi kuantitas data obat saat ini.']);
            }

            if ($request->qty_return) {

                // update data retur 
                $newDataRetur = [
                    'qty_return' => $retur->qty_return + $request->qty_return,
                    'retur_category_id' => $request->retur_category_id
                ];
                $retur->update($newDataRetur);

                // menambahkan histori data obat 
                HistoryItem::create([
                    'medicine_id' => $retur->medicine_id,
                    'user_id' => Auth::id(),
                    'stock' => $medicine->stock ,
                    'stockChange' => $request->qty_return * -1,
                    'last_stock' => $medicine->stock + ($request->qty_return * -1),
                    'tipe' => 'Edit Retur'
                ]);

                // menambahkan/mengurangi kuantitas obat yang salah diretur
                Medicine::where('id', $retur->medicine_id)
                    ->update([
                        'stock' => $medicine->stock + ($request->qty_return * -1)
                    ]);
            } else if ($request->rollback == TRUE) {

                // menambahkan histori data obat 
                HistoryItem::create([
                    'medicine_id' => $retur->medicine_id,
                    'user_id' => Auth::id(),
                    'stock' => $medicine->stock,
                    'stockChange' => $retur->qty_return,
                    'last_stock' => $medicine->stock + $retur->qty_return,
                    'tipe' => 'Ditetapkan berdasarkan salah input retur'
                ]);

                // mengembalikan kuantitas obat yang salah retur 
                Medicine::where('id', $retur->medicine_id)
                    ->update([
                        'stock' => $medicine->stock + $retur->qty_return
                    ]);

                // delete data retur 
                $retur->find($id)->delete();

                $message = "Data berhasil dirollback";
            } else {
                $newDataRetur = [
                    'retur_category_id' => $request->retur_category_id
                ];
                $retur->update($newDataRetur);
            }


            DB::commit();
            return redirect()->route('medicine-return.index')->with('success', $message);
        } catch (\Exeception $e) {
            DB::rollback();
            return redirect()->route('medicine-return.index')->with('error', $e);
        }
    }

    public function checkExpired()
    {
        Medicine::where('expired', '<=', Carbon::now()->toDateString())->update([
            'is_expired' => TRUE
        ]);
        Medicine::where('expired', '>=', Carbon::now()->toDateString())->update([
            'is_expired' => FALSE
        ]);
        $message = 'Data Obat Kadaluarsa Berhasil Dicek';
        return redirect()->route('medicine-return.index')->with('success', $message);
    }
}
