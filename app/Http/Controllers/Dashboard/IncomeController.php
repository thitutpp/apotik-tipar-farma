<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Model\Income;
use App\Model\IncomeCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class IncomeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:Akses Manajemen Biaya', ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'trash', 'restore', 'permanentDelete']]);
    }

    // function __construct()
    // {
    //     $this->middleware('permission:income-list', ['only' => ['index']]);
    //     $this->middleware('permission:income-create', ['only' => ['create', 'store']]);
    //     $this->middleware('permission:income-edit', ['only' => ['edit', 'update']]);
    //     $this->middleware('permission:income-delete', ['only' => ['destroy']]);
    //     $this->middleware('permission:income-trash', ['only' => ['trash']]);
    //     $this->middleware('permission:income-restore', ['only' => ['restore']]);
    //     $this->middleware('permission:income-permanentDelete', ['only' => ['permanentDelete']]);
    // }

    public function index()
    {
        $incomes = Income::with(['income_category'])->get();
        return view('dashboard.financialManagement.incomes.index', compact('incomes'))->with('no');
    }

    public function create()
    {
        $income_categories = IncomeCategory::all()->pluck('name', 'id')->prepend(trans('Pilih Kategori'), '');
        return view('dashboard.financialManagement.incomes.create', compact('income_categories'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'income_category_id' => 'required',
            'entry_date' => 'required',
            'amount' => 'required',
            'description' => '',
        ]);

        Income::create([
            'user_id' => Auth::id(),
            'income_category_id' => $request->income_category_id,
            'entry_date' => $request->entry_date,
            'amount' => $request->amount,
            'description' => $request->description,
        ]);
        $message = 'Data pendapatan berhasil ditambahkan';
        return redirect()->route('incomes.index')->with('success', $message);
    }


    public function show(Income $income)
    {
        $income->load('income_category');
        return view('dashboard.financialManagement.incomes.show', compact('income'));
    }


    public function edit($id)
    {
        $incomes = Income::find($id);
        $income_categories = IncomeCategory::all()->pluck('name', 'id')->prepend(trans('Pilih Kategori'), '');
        $incomes->load('income_category');
        return view('dashboard.financialManagement.incomes.edit', compact('income_categories', 'incomes'));
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'income_category_id' => '',
            'entry_date' => 'required',
            'amount' => 'required',
            'description' => '',

        ]);

        $incomes  = Income::find($id);
        $incomes->income_category_id = $request->input('income_category_id');
        $incomes->entry_date = $request->input('entry_date');
        $incomes->amount = $request->input('amount');
        $incomes->description = $request->input('description');
        $incomes->save();
        $message = 'Data pendapatan berhasil diperbaruhi';
        return redirect()->route('incomes.index')->with('success', $message);
    }


    public function destroy($id)
    {
        $incomes = Income::find($id);
        $incomes->delete();
        $message = "Data pendapatan berhasil dihapus";
        return redirect()->back()->with('success', $message);
    }

    public function trash()
    {
        $incomes = Income::onlyTrashed()->get();
        return view('dashboard.financialManagement.incomes.trash', compact('incomes'))->with('no');
    }

    public function restore($id)
    {
        $incomes = Income::onlyTrashed()->where('id', $id);
        $incomes->restore();
        $message = "Data pendapatan berhasil dipulihkan";
        return redirect()->route('incomes.index')->with('success', $message);
    }

    public function permanentDelete($id)
    {
        $expense = Income::onlyTrashed()->where('id', $id);
        $expense->forceDelete();
        $message = "Data pendapatan berhasil dihapus permanent";
        return redirect()->back()->with('success', $message);
    }
}
