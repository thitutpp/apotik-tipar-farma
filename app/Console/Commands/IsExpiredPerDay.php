<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\Model\Medicine;

class IsExpiredPerDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Memberikan nilai TRUE kedalam field is_expired tabel Medicine';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Medicine::where('expired', '>=' , Carbon::now()->toDateString())->update([
            'is_expired' => TRUE
        ]);
// 
        // Artisan::command('artisan up');
    }
}
