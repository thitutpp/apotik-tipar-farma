<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\IsExpiredPerDay::class,
        // Commands\HourlyUpdate::class
        // commands\ThreeMonthlyUpdate::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('threeMonth:update')->monthly(3);
        $schedule->command('expired:day')->everyMinute();
        // $schedule->command('expired:day --force')->everyMinute();
        // $schedule->command('hour:update')->hourly();
        // $schedule->call(function () {
        //     Medicine::where('expired', '>=' , Carbon::now()->toDateString())->update([
        //         'is_expired' => TRUE
        //     ]);
    
        // })->everyMinute();
    
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        // $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
